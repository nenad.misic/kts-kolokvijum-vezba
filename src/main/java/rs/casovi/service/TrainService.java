package rs.casovi.service;

import rs.casovi.domain.Train;

import java.util.List;
import java.util.Optional;

/**
 * Service Interface for managing {@link Train}.
 */
public interface TrainService {

    /**
     * Save a train.
     *
     * @param train the entity to save.
     * @return the persisted entity.
     */
    Train save(Train train);

    /**
     * Get all the trains.
     *
     * @return the list of entities.
     */
    List<Train> findAll();


    /**
     * Get the "id" train.
     *
     * @param id the id of the entity.
     * @return the entity.
     */
    Optional<Train> findOne(Long id);

    /**
     * Delete the "id" train.
     *
     * @param id the id of the entity.
     */
    void delete(Long id);
}
