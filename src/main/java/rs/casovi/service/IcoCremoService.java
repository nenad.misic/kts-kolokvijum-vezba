package rs.casovi.service;

import rs.casovi.domain.IcoCremo;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import java.util.Optional;

/**
 * Service Interface for managing {@link IcoCremo}.
 */
public interface IcoCremoService {

    /**
     * Save a icoCremo.
     *
     * @param icoCremo the entity to save.
     * @return the persisted entity.
     */
    IcoCremo save(IcoCremo icoCremo);

    /**
     * Get all the icoCremos.
     *
     * @param pageable the pagination information.
     * @return the list of entities.
     */
    Page<IcoCremo> findAll(Pageable pageable);


    /**
     * Get the "id" icoCremo.
     *
     * @param id the id of the entity.
     * @return the entity.
     */
    Optional<IcoCremo> findOne(Long id);

    /**
     * Delete the "id" icoCremo.
     *
     * @param id the id of the entity.
     */
    void delete(Long id);
}
