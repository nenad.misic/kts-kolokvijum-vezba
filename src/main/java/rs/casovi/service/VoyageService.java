package rs.casovi.service;

import rs.casovi.domain.Voyage;

import java.util.List;
import java.util.Optional;

/**
 * Service Interface for managing {@link Voyage}.
 */
public interface VoyageService {

    /**
     * Save a voyage.
     *
     * @param voyage the entity to save.
     * @return the persisted entity.
     */
    Voyage save(Voyage voyage);

    /**
     * Get all the voyages.
     *
     * @return the list of entities.
     */
    List<Voyage> findAll();


    /**
     * Get the "id" voyage.
     *
     * @param id the id of the entity.
     * @return the entity.
     */
    Optional<Voyage> findOne(Long id);

    /**
     * Delete the "id" voyage.
     *
     * @param id the id of the entity.
     */
    void delete(Long id);
}
