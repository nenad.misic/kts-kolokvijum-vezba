package rs.casovi.service.impl;

import rs.casovi.service.VoyageService;
import rs.casovi.domain.Voyage;
import rs.casovi.repository.VoyageRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Optional;

/**
 * Service Implementation for managing {@link Voyage}.
 */
@Service
@Transactional
public class VoyageServiceImpl implements VoyageService {

    private final Logger log = LoggerFactory.getLogger(VoyageServiceImpl.class);

    private final VoyageRepository voyageRepository;

    public VoyageServiceImpl(VoyageRepository voyageRepository) {
        this.voyageRepository = voyageRepository;
    }

    /**
     * Save a voyage.
     *
     * @param voyage the entity to save.
     * @return the persisted entity.
     */
    @Override
    public Voyage save(Voyage voyage) {
        log.debug("Request to save Voyage : {}", voyage);
        return voyageRepository.save(voyage);
    }

    /**
     * Get all the voyages.
     *
     * @return the list of entities.
     */
    @Override
    @Transactional(readOnly = true)
    public List<Voyage> findAll() {
        log.debug("Request to get all Voyages");
        return voyageRepository.findAll();
    }


    /**
     * Get one voyage by id.
     *
     * @param id the id of the entity.
     * @return the entity.
     */
    @Override
    @Transactional(readOnly = true)
    public Optional<Voyage> findOne(Long id) {
        log.debug("Request to get Voyage : {}", id);
        return voyageRepository.findById(id);
    }

    /**
     * Delete the voyage by id.
     *
     * @param id the id of the entity.
     */
    @Override
    public void delete(Long id) {
        log.debug("Request to delete Voyage : {}", id);
        voyageRepository.deleteById(id);
    }
}
