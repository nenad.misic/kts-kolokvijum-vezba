package rs.casovi.service.impl;

import rs.casovi.service.IcoCremoService;
import rs.casovi.domain.IcoCremo;
import rs.casovi.repository.IcoCremoRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Optional;

/**
 * Service Implementation for managing {@link IcoCremo}.
 */
@Service
@Transactional
public class IcoCremoServiceImpl implements IcoCremoService {

    private final Logger log = LoggerFactory.getLogger(IcoCremoServiceImpl.class);

    private final IcoCremoRepository icoCremoRepository;

    public IcoCremoServiceImpl(IcoCremoRepository icoCremoRepository) {
        this.icoCremoRepository = icoCremoRepository;
    }

    /**
     * Save a icoCremo.
     *
     * @param icoCremo the entity to save.
     * @return the persisted entity.
     */
    @Override
    public IcoCremo save(IcoCremo icoCremo) {
        log.debug("Request to save IcoCremo : {}", icoCremo);
        return icoCremoRepository.save(icoCremo);
    }

    /**
     * Get all the icoCremos.
     *
     * @param pageable the pagination information.
     * @return the list of entities.
     */
    @Override
    @Transactional(readOnly = true)
    public Page<IcoCremo> findAll(Pageable pageable) {
        log.debug("Request to get all IcoCremos");
        return icoCremoRepository.findAll(pageable);
    }


    /**
     * Get one icoCremo by id.
     *
     * @param id the id of the entity.
     * @return the entity.
     */
    @Override
    @Transactional(readOnly = true)
    public Optional<IcoCremo> findOne(Long id) {
        log.debug("Request to get IcoCremo : {}", id);
        return icoCremoRepository.findById(id);
    }

    /**
     * Delete the icoCremo by id.
     *
     * @param id the id of the entity.
     */
    @Override
    public void delete(Long id) {
        log.debug("Request to delete IcoCremo : {}", id);
        icoCremoRepository.deleteById(id);
    }
}
