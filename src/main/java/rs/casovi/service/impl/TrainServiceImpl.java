package rs.casovi.service.impl;

import rs.casovi.service.TrainService;
import rs.casovi.domain.Train;
import rs.casovi.repository.TrainRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Optional;

/**
 * Service Implementation for managing {@link Train}.
 */
@Service
@Transactional
public class TrainServiceImpl implements TrainService {

    private final Logger log = LoggerFactory.getLogger(TrainServiceImpl.class);

    private final TrainRepository trainRepository;

    public TrainServiceImpl(TrainRepository trainRepository) {
        this.trainRepository = trainRepository;
    }

    /**
     * Save a train.
     *
     * @param train the entity to save.
     * @return the persisted entity.
     */
    @Override
    public Train save(Train train) {
        log.debug("Request to save Train : {}", train);
        return trainRepository.save(train);
    }

    /**
     * Get all the trains.
     *
     * @return the list of entities.
     */
    @Override
    @Transactional(readOnly = true)
    public List<Train> findAll() {
        log.debug("Request to get all Trains");
        return trainRepository.findAll();
    }


    /**
     * Get one train by id.
     *
     * @param id the id of the entity.
     * @return the entity.
     */
    @Override
    @Transactional(readOnly = true)
    public Optional<Train> findOne(Long id) {
        log.debug("Request to get Train : {}", id);
        return trainRepository.findById(id);
    }

    /**
     * Delete the train by id.
     *
     * @param id the id of the entity.
     */
    @Override
    public void delete(Long id) {
        log.debug("Request to delete Train : {}", id);
        trainRepository.deleteById(id);
    }
}
