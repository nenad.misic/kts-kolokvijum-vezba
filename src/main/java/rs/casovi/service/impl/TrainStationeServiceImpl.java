package rs.casovi.service.impl;

import rs.casovi.service.TrainStationeService;
import rs.casovi.domain.TrainStatione;
import rs.casovi.repository.TrainStationeRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Optional;

/**
 * Service Implementation for managing {@link TrainStatione}.
 */
@Service
@Transactional
public class TrainStationeServiceImpl implements TrainStationeService {

    private final Logger log = LoggerFactory.getLogger(TrainStationeServiceImpl.class);

    private final TrainStationeRepository trainStationeRepository;

    public TrainStationeServiceImpl(TrainStationeRepository trainStationeRepository) {
        this.trainStationeRepository = trainStationeRepository;
    }

    /**
     * Save a trainStatione.
     *
     * @param trainStatione the entity to save.
     * @return the persisted entity.
     */
    @Override
    public TrainStatione save(TrainStatione trainStatione) {
        log.debug("Request to save TrainStatione : {}", trainStatione);
        return trainStationeRepository.save(trainStatione);
    }

    /**
     * Get all the trainStationes.
     *
     * @param pageable the pagination information.
     * @return the list of entities.
     */
    @Override
    @Transactional(readOnly = true)
    public Page<TrainStatione> findAll(Pageable pageable) {
        log.debug("Request to get all TrainStationes");
        return trainStationeRepository.findAll(pageable);
    }


    /**
     * Get one trainStatione by id.
     *
     * @param id the id of the entity.
     * @return the entity.
     */
    @Override
    @Transactional(readOnly = true)
    public Optional<TrainStatione> findOne(Long id) {
        log.debug("Request to get TrainStatione : {}", id);
        return trainStationeRepository.findById(id);
    }

    /**
     * Delete the trainStatione by id.
     *
     * @param id the id of the entity.
     */
    @Override
    public void delete(Long id) {
        log.debug("Request to delete TrainStatione : {}", id);
        trainStationeRepository.deleteById(id);
    }
}
