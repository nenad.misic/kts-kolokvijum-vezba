package rs.casovi.service;

import rs.casovi.domain.TrainStatione;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import java.util.Optional;

/**
 * Service Interface for managing {@link TrainStatione}.
 */
public interface TrainStationeService {

    /**
     * Save a trainStatione.
     *
     * @param trainStatione the entity to save.
     * @return the persisted entity.
     */
    TrainStatione save(TrainStatione trainStatione);

    /**
     * Get all the trainStationes.
     *
     * @param pageable the pagination information.
     * @return the list of entities.
     */
    Page<TrainStatione> findAll(Pageable pageable);


    /**
     * Get the "id" trainStatione.
     *
     * @param id the id of the entity.
     * @return the entity.
     */
    Optional<TrainStatione> findOne(Long id);

    /**
     * Delete the "id" trainStatione.
     *
     * @param id the id of the entity.
     */
    void delete(Long id);
}
