package rs.casovi.domain;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import javax.persistence.*;
import javax.validation.constraints.*;

import java.io.Serializable;

/**
 * A Train.
 */
@Entity
@Table(name = "train")
public class Train implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "sequenceGenerator")
    @SequenceGenerator(name = "sequenceGenerator")
    private Long id;

    @NotNull
    @Column(name = "name", nullable = false)
    private String name;

    @Column(name = "num_of_cars")
    private Integer numOfCars;

    @ManyToOne
    @JsonIgnoreProperties("trains")
    private Voyage voyage;

    // jhipster-needle-entity-add-field - JHipster will add fields here, do not remove
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public Train name(String name) {
        this.name = name;
        return this;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Integer getNumOfCars() {
        return numOfCars;
    }

    public Train numOfCars(Integer numOfCars) {
        this.numOfCars = numOfCars;
        return this;
    }

    public void setNumOfCars(Integer numOfCars) {
        this.numOfCars = numOfCars;
    }

    public Voyage getVoyage() {
        return voyage;
    }

    public Train voyage(Voyage voyage) {
        this.voyage = voyage;
        return this;
    }

    public void setVoyage(Voyage voyage) {
        this.voyage = voyage;
    }
    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here, do not remove

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof Train)) {
            return false;
        }
        return id != null && id.equals(((Train) o).id);
    }

    @Override
    public int hashCode() {
        return 31;
    }

    @Override
    public String toString() {
        return "Train{" +
            "id=" + getId() +
            ", name='" + getName() + "'" +
            ", numOfCars=" + getNumOfCars() +
            "}";
    }
}
