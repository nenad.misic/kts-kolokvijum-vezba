package rs.casovi.domain;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import javax.persistence.*;

import java.io.Serializable;
import java.time.Instant;
import java.util.HashSet;
import java.util.Set;

/**
 * A Voyage.
 */
@Entity
@Table(name = "voyage")
public class Voyage implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "sequenceGenerator")
    @SequenceGenerator(name = "sequenceGenerator")
    private Long id;

    @Column(name = "start_date")
    private Instant startDate;

    @Column(name = "end_date")
    private Instant endDate;

    @OneToMany(mappedBy = "voyage")
    private Set<IcoCremo> icecremos = new HashSet<>();

    @OneToMany(mappedBy = "voyage")
    private Set<Train> trains = new HashSet<>();

    @ManyToOne
    @JsonIgnoreProperties("voyages")
    private TrainStatione from;

    @ManyToOne
    @JsonIgnoreProperties("voyages")
    private TrainStatione to;

    // jhipster-needle-entity-add-field - JHipster will add fields here, do not remove
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Instant getStartDate() {
        return startDate;
    }

    public Voyage startDate(Instant startDate) {
        this.startDate = startDate;
        return this;
    }

    public void setStartDate(Instant startDate) {
        this.startDate = startDate;
    }

    public Instant getEndDate() {
        return endDate;
    }

    public Voyage endDate(Instant endDate) {
        this.endDate = endDate;
        return this;
    }

    public void setEndDate(Instant endDate) {
        this.endDate = endDate;
    }

    public Set<IcoCremo> getIcecremos() {
        return icecremos;
    }

    public Voyage icecremos(Set<IcoCremo> icoCremos) {
        this.icecremos = icoCremos;
        return this;
    }

    public Voyage addIcecremo(IcoCremo icoCremo) {
        this.icecremos.add(icoCremo);
        icoCremo.setVoyage(this);
        return this;
    }

    public Voyage removeIcecremo(IcoCremo icoCremo) {
        this.icecremos.remove(icoCremo);
        icoCremo.setVoyage(null);
        return this;
    }

    public void setIcecremos(Set<IcoCremo> icoCremos) {
        this.icecremos = icoCremos;
    }

    public Set<Train> getTrains() {
        return trains;
    }

    public Voyage trains(Set<Train> trains) {
        this.trains = trains;
        return this;
    }

    public Voyage addTrain(Train train) {
        this.trains.add(train);
        train.setVoyage(this);
        return this;
    }

    public Voyage removeTrain(Train train) {
        this.trains.remove(train);
        train.setVoyage(null);
        return this;
    }

    public void setTrains(Set<Train> trains) {
        this.trains = trains;
    }

    public TrainStatione getFrom() {
        return from;
    }

    public Voyage from(TrainStatione trainStatione) {
        this.from = trainStatione;
        return this;
    }

    public void setFrom(TrainStatione trainStatione) {
        this.from = trainStatione;
    }

    public TrainStatione getTo() {
        return to;
    }

    public Voyage to(TrainStatione trainStatione) {
        this.to = trainStatione;
        return this;
    }

    public void setTo(TrainStatione trainStatione) {
        this.to = trainStatione;
    }
    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here, do not remove

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof Voyage)) {
            return false;
        }
        return id != null && id.equals(((Voyage) o).id);
    }

    @Override
    public int hashCode() {
        return 31;
    }

    @Override
    public String toString() {
        return "Voyage{" +
            "id=" + getId() +
            ", startDate='" + getStartDate() + "'" +
            ", endDate='" + getEndDate() + "'" +
            "}";
    }
}
