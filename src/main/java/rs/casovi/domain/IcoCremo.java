package rs.casovi.domain;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import javax.persistence.*;

import java.io.Serializable;

/**
 * A IcoCremo.
 */
@Entity
@Table(name = "ico_cremo")
public class IcoCremo implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "sequenceGenerator")
    @SequenceGenerator(name = "sequenceGenerator")
    private Long id;

    @Column(name = "name")
    private String name;

    @Column(name = "num_of_kugeln")
    private Integer numOfKugeln;

    @Column(name = "keks_kornet_keks")
    private Boolean keksKornetKeks;

    @Column(name = "price")
    private Float price;

    @ManyToOne
    @JsonIgnoreProperties("icecremos")
    private Voyage voyage;

    // jhipster-needle-entity-add-field - JHipster will add fields here, do not remove
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public IcoCremo name(String name) {
        this.name = name;
        return this;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Integer getNumOfKugeln() {
        return numOfKugeln;
    }

    public IcoCremo numOfKugeln(Integer numOfKugeln) {
        this.numOfKugeln = numOfKugeln;
        return this;
    }

    public void setNumOfKugeln(Integer numOfKugeln) {
        this.numOfKugeln = numOfKugeln;
    }

    public Boolean isKeksKornetKeks() {
        return keksKornetKeks;
    }

    public IcoCremo keksKornetKeks(Boolean keksKornetKeks) {
        this.keksKornetKeks = keksKornetKeks;
        return this;
    }

    public void setKeksKornetKeks(Boolean keksKornetKeks) {
        this.keksKornetKeks = keksKornetKeks;
    }

    public Float getPrice() {
        return price;
    }

    public IcoCremo price(Float price) {
        this.price = price;
        return this;
    }

    public void setPrice(Float price) {
        this.price = price;
    }

    public Voyage getVoyage() {
        return voyage;
    }

    public IcoCremo voyage(Voyage voyage) {
        this.voyage = voyage;
        return this;
    }

    public void setVoyage(Voyage voyage) {
        this.voyage = voyage;
    }
    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here, do not remove

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof IcoCremo)) {
            return false;
        }
        return id != null && id.equals(((IcoCremo) o).id);
    }

    @Override
    public int hashCode() {
        return 31;
    }

    @Override
    public String toString() {
        return "IcoCremo{" +
            "id=" + getId() +
            ", name='" + getName() + "'" +
            ", numOfKugeln=" + getNumOfKugeln() +
            ", keksKornetKeks='" + isKeksKornetKeks() + "'" +
            ", price=" + getPrice() +
            "}";
    }
}
