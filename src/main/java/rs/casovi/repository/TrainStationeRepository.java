package rs.casovi.repository;
import rs.casovi.domain.TrainStatione;
import org.springframework.data.jpa.repository.*;
import org.springframework.stereotype.Repository;


/**
 * Spring Data  repository for the TrainStatione entity.
 */
@SuppressWarnings("unused")
@Repository
public interface TrainStationeRepository extends JpaRepository<TrainStatione, Long> {

}
