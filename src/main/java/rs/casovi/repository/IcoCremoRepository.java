package rs.casovi.repository;
import rs.casovi.domain.IcoCremo;
import org.springframework.data.jpa.repository.*;
import org.springframework.stereotype.Repository;


/**
 * Spring Data  repository for the IcoCremo entity.
 */
@SuppressWarnings("unused")
@Repository
public interface IcoCremoRepository extends JpaRepository<IcoCremo, Long> {

}
