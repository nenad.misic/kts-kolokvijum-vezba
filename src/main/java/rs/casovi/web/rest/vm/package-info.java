/**
 * View Models used by Spring MVC REST controllers.
 */
package rs.casovi.web.rest.vm;
