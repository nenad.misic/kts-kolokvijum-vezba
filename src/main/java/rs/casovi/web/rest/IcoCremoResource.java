package rs.casovi.web.rest;

import rs.casovi.domain.IcoCremo;
import rs.casovi.service.IcoCremoService;
import rs.casovi.web.rest.errors.BadRequestAlertException;

import io.github.jhipster.web.util.HeaderUtil;
import io.github.jhipster.web.util.PaginationUtil;
import io.github.jhipster.web.util.ResponseUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.net.URI;
import java.net.URISyntaxException;

import java.util.List;
import java.util.Optional;

/**
 * REST controller for managing {@link rs.casovi.domain.IcoCremo}.
 */
@RestController
@RequestMapping("/api")
public class IcoCremoResource {

    private final Logger log = LoggerFactory.getLogger(IcoCremoResource.class);

    private static final String ENTITY_NAME = "icoCremo";

    @Value("${jhipster.clientApp.name}")
    private String applicationName;

    private final IcoCremoService icoCremoService;

    public IcoCremoResource(IcoCremoService icoCremoService) {
        this.icoCremoService = icoCremoService;
    }

    /**
     * {@code POST  /ico-cremos} : Create a new icoCremo.
     *
     * @param icoCremo the icoCremo to create.
     * @return the {@link ResponseEntity} with status {@code 201 (Created)} and with body the new icoCremo, or with status {@code 400 (Bad Request)} if the icoCremo has already an ID.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PostMapping("/ico-cremos")
    public ResponseEntity<IcoCremo> createIcoCremo(@RequestBody IcoCremo icoCremo) throws URISyntaxException {
        log.debug("REST request to save IcoCremo : {}", icoCremo);
        if (icoCremo.getId() != null) {
            throw new BadRequestAlertException("A new icoCremo cannot already have an ID", ENTITY_NAME, "idexists");
        }
        IcoCremo result = icoCremoService.save(icoCremo);
        return ResponseEntity.created(new URI("/api/ico-cremos/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(applicationName, true, ENTITY_NAME, result.getId().toString()))
            .body(result);
    }

    /**
     * {@code PUT  /ico-cremos} : Updates an existing icoCremo.
     *
     * @param icoCremo the icoCremo to update.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the updated icoCremo,
     * or with status {@code 400 (Bad Request)} if the icoCremo is not valid,
     * or with status {@code 500 (Internal Server Error)} if the icoCremo couldn't be updated.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PutMapping("/ico-cremos")
    public ResponseEntity<IcoCremo> updateIcoCremo(@RequestBody IcoCremo icoCremo) throws URISyntaxException {
        log.debug("REST request to update IcoCremo : {}", icoCremo);
        if (icoCremo.getId() == null) {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
        }
        IcoCremo result = icoCremoService.save(icoCremo);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert(applicationName, true, ENTITY_NAME, icoCremo.getId().toString()))
            .body(result);
    }

    /**
     * {@code GET  /ico-cremos} : get all the icoCremos.
     *

     * @param pageable the pagination information.

     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and the list of icoCremos in body.
     */
    @GetMapping("/ico-cremos")
    public ResponseEntity<List<IcoCremo>> getAllIcoCremos(Pageable pageable) {
        log.debug("REST request to get a page of IcoCremos");
        Page<IcoCremo> page = icoCremoService.findAll(pageable);
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(ServletUriComponentsBuilder.fromCurrentRequest(), page);
        return ResponseEntity.ok().headers(headers).body(page.getContent());
    }

    /**
     * {@code GET  /ico-cremos/:id} : get the "id" icoCremo.
     *
     * @param id the id of the icoCremo to retrieve.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the icoCremo, or with status {@code 404 (Not Found)}.
     */
    @GetMapping("/ico-cremos/{id}")
    public ResponseEntity<IcoCremo> getIcoCremo(@PathVariable Long id) {
        log.debug("REST request to get IcoCremo : {}", id);
        Optional<IcoCremo> icoCremo = icoCremoService.findOne(id);
        return ResponseUtil.wrapOrNotFound(icoCremo);
    }

    /**
     * {@code DELETE  /ico-cremos/:id} : delete the "id" icoCremo.
     *
     * @param id the id of the icoCremo to delete.
     * @return the {@link ResponseEntity} with status {@code 204 (NO_CONTENT)}.
     */
    @DeleteMapping("/ico-cremos/{id}")
    public ResponseEntity<Void> deleteIcoCremo(@PathVariable Long id) {
        log.debug("REST request to delete IcoCremo : {}", id);
        icoCremoService.delete(id);
        return ResponseEntity.noContent().headers(HeaderUtil.createEntityDeletionAlert(applicationName, true, ENTITY_NAME, id.toString())).build();
    }
}
