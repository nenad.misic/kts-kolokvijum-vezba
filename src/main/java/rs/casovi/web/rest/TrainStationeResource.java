package rs.casovi.web.rest;

import rs.casovi.domain.TrainStatione;
import rs.casovi.service.TrainStationeService;
import rs.casovi.web.rest.errors.BadRequestAlertException;

import io.github.jhipster.web.util.HeaderUtil;
import io.github.jhipster.web.util.PaginationUtil;
import io.github.jhipster.web.util.ResponseUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.net.URI;
import java.net.URISyntaxException;

import java.util.List;
import java.util.Optional;

/**
 * REST controller for managing {@link rs.casovi.domain.TrainStatione}.
 */
@RestController
@RequestMapping("/api")
public class TrainStationeResource {

    private final Logger log = LoggerFactory.getLogger(TrainStationeResource.class);

    private static final String ENTITY_NAME = "trainStatione";

    @Value("${jhipster.clientApp.name}")
    private String applicationName;

    private final TrainStationeService trainStationeService;

    public TrainStationeResource(TrainStationeService trainStationeService) {
        this.trainStationeService = trainStationeService;
    }

    /**
     * {@code POST  /train-stationes} : Create a new trainStatione.
     *
     * @param trainStatione the trainStatione to create.
     * @return the {@link ResponseEntity} with status {@code 201 (Created)} and with body the new trainStatione, or with status {@code 400 (Bad Request)} if the trainStatione has already an ID.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PostMapping("/train-stationes")
    public ResponseEntity<TrainStatione> createTrainStatione(@Valid @RequestBody TrainStatione trainStatione) throws URISyntaxException {
        log.debug("REST request to save TrainStatione : {}", trainStatione);
        if (trainStatione.getId() != null) {
            throw new BadRequestAlertException("A new trainStatione cannot already have an ID", ENTITY_NAME, "idexists");
        }
        TrainStatione result = trainStationeService.save(trainStatione);
        return ResponseEntity.created(new URI("/api/train-stationes/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(applicationName, true, ENTITY_NAME, result.getId().toString()))
            .body(result);
    }

    /**
     * {@code PUT  /train-stationes} : Updates an existing trainStatione.
     *
     * @param trainStatione the trainStatione to update.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the updated trainStatione,
     * or with status {@code 400 (Bad Request)} if the trainStatione is not valid,
     * or with status {@code 500 (Internal Server Error)} if the trainStatione couldn't be updated.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PutMapping("/train-stationes")
    public ResponseEntity<TrainStatione> updateTrainStatione(@Valid @RequestBody TrainStatione trainStatione) throws URISyntaxException {
        log.debug("REST request to update TrainStatione : {}", trainStatione);
        if (trainStatione.getId() == null) {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
        }
        TrainStatione result = trainStationeService.save(trainStatione);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert(applicationName, true, ENTITY_NAME, trainStatione.getId().toString()))
            .body(result);
    }

    /**
     * {@code GET  /train-stationes} : get all the trainStationes.
     *

     * @param pageable the pagination information.

     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and the list of trainStationes in body.
     */
    @GetMapping("/train-stationes")
    public ResponseEntity<List<TrainStatione>> getAllTrainStationes(Pageable pageable) {
        log.debug("REST request to get a page of TrainStationes");
        Page<TrainStatione> page = trainStationeService.findAll(pageable);
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(ServletUriComponentsBuilder.fromCurrentRequest(), page);
        return ResponseEntity.ok().headers(headers).body(page.getContent());
    }

    /**
     * {@code GET  /train-stationes/:id} : get the "id" trainStatione.
     *
     * @param id the id of the trainStatione to retrieve.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the trainStatione, or with status {@code 404 (Not Found)}.
     */
    @GetMapping("/train-stationes/{id}")
    public ResponseEntity<TrainStatione> getTrainStatione(@PathVariable Long id) {
        log.debug("REST request to get TrainStatione : {}", id);
        Optional<TrainStatione> trainStatione = trainStationeService.findOne(id);
        return ResponseUtil.wrapOrNotFound(trainStatione);
    }

    /**
     * {@code DELETE  /train-stationes/:id} : delete the "id" trainStatione.
     *
     * @param id the id of the trainStatione to delete.
     * @return the {@link ResponseEntity} with status {@code 204 (NO_CONTENT)}.
     */
    @DeleteMapping("/train-stationes/{id}")
    public ResponseEntity<Void> deleteTrainStatione(@PathVariable Long id) {
        log.debug("REST request to delete TrainStatione : {}", id);
        trainStationeService.delete(id);
        return ResponseEntity.noContent().headers(HeaderUtil.createEntityDeletionAlert(applicationName, true, ENTITY_NAME, id.toString())).build();
    }
}
