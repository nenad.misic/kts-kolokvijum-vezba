import { Injectable } from '@angular/core';
import { HttpClient, HttpResponse } from '@angular/common/http';
import { Observable } from 'rxjs';

import { SERVER_API_URL } from 'app/app.constants';
import { createRequestOption } from 'app/shared/util/request-util';
import { IIcoCremo } from 'app/shared/model/ico-cremo.model';

type EntityResponseType = HttpResponse<IIcoCremo>;
type EntityArrayResponseType = HttpResponse<IIcoCremo[]>;

@Injectable({ providedIn: 'root' })
export class IcoCremoService {
  public resourceUrl = SERVER_API_URL + 'api/ico-cremos';

  constructor(protected http: HttpClient) {}

  create(icoCremo: IIcoCremo): Observable<EntityResponseType> {
    return this.http.post<IIcoCremo>(this.resourceUrl, icoCremo, { observe: 'response' });
  }

  update(icoCremo: IIcoCremo): Observable<EntityResponseType> {
    return this.http.put<IIcoCremo>(this.resourceUrl, icoCremo, { observe: 'response' });
  }

  find(id: number): Observable<EntityResponseType> {
    return this.http.get<IIcoCremo>(`${this.resourceUrl}/${id}`, { observe: 'response' });
  }

  query(req?: any): Observable<EntityArrayResponseType> {
    const options = createRequestOption(req);
    return this.http.get<IIcoCremo[]>(this.resourceUrl, { params: options, observe: 'response' });
  }

  delete(id: number): Observable<HttpResponse<any>> {
    return this.http.delete<any>(`${this.resourceUrl}/${id}`, { observe: 'response' });
  }
}
