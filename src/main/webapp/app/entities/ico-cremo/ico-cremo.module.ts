import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';

import { IcyTrainSharedModule } from 'app/shared/shared.module';
import { IcoCremoComponent } from './ico-cremo.component';
import { IcoCremoDetailComponent } from './ico-cremo-detail.component';
import { IcoCremoUpdateComponent } from './ico-cremo-update.component';
import { IcoCremoDeleteDialogComponent } from './ico-cremo-delete-dialog.component';
import { icoCremoRoute } from './ico-cremo.route';

@NgModule({
  imports: [IcyTrainSharedModule, RouterModule.forChild(icoCremoRoute)],
  declarations: [IcoCremoComponent, IcoCremoDetailComponent, IcoCremoUpdateComponent, IcoCremoDeleteDialogComponent],
  entryComponents: [IcoCremoDeleteDialogComponent]
})
export class IcyTrainIcoCremoModule {}
