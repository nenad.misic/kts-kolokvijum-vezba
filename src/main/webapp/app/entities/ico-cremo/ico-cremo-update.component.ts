import { Component, OnInit } from '@angular/core';
// eslint-disable-next-line @typescript-eslint/no-unused-vars
import { HttpResponse, HttpErrorResponse } from '@angular/common/http';
// eslint-disable-next-line @typescript-eslint/no-unused-vars
import { FormBuilder, Validators } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { Observable } from 'rxjs';
import { JhiAlertService } from 'ng-jhipster';
import { IIcoCremo, IcoCremo } from 'app/shared/model/ico-cremo.model';
import { IcoCremoService } from './ico-cremo.service';
import { IVoyage } from 'app/shared/model/voyage.model';
import { VoyageService } from 'app/entities/voyage/voyage.service';

@Component({
  selector: 'jhi-ico-cremo-update',
  templateUrl: './ico-cremo-update.component.html'
})
export class IcoCremoUpdateComponent implements OnInit {
  isSaving: boolean;

  voyages: IVoyage[];

  editForm = this.fb.group({
    id: [],
    name: [],
    numOfKugeln: [],
    keksKornetKeks: [],
    price: [],
    voyage: []
  });

  constructor(
    protected jhiAlertService: JhiAlertService,
    protected icoCremoService: IcoCremoService,
    protected voyageService: VoyageService,
    protected activatedRoute: ActivatedRoute,
    private fb: FormBuilder
  ) {}

  ngOnInit() {
    this.isSaving = false;
    this.activatedRoute.data.subscribe(({ icoCremo }) => {
      this.updateForm(icoCremo);
    });
    this.voyageService
      .query()
      .subscribe((res: HttpResponse<IVoyage[]>) => (this.voyages = res.body), (res: HttpErrorResponse) => this.onError(res.message));
  }

  updateForm(icoCremo: IIcoCremo) {
    this.editForm.patchValue({
      id: icoCremo.id,
      name: icoCremo.name,
      numOfKugeln: icoCremo.numOfKugeln,
      keksKornetKeks: icoCremo.keksKornetKeks,
      price: icoCremo.price,
      voyage: icoCremo.voyage
    });
  }

  previousState() {
    window.history.back();
  }

  save() {
    this.isSaving = true;
    const icoCremo = this.createFromForm();
    if (icoCremo.id !== undefined) {
      this.subscribeToSaveResponse(this.icoCremoService.update(icoCremo));
    } else {
      this.subscribeToSaveResponse(this.icoCremoService.create(icoCremo));
    }
  }

  private createFromForm(): IIcoCremo {
    return {
      ...new IcoCremo(),
      id: this.editForm.get(['id']).value,
      name: this.editForm.get(['name']).value,
      numOfKugeln: this.editForm.get(['numOfKugeln']).value,
      keksKornetKeks: this.editForm.get(['keksKornetKeks']).value,
      price: this.editForm.get(['price']).value,
      voyage: this.editForm.get(['voyage']).value
    };
  }

  protected subscribeToSaveResponse(result: Observable<HttpResponse<IIcoCremo>>) {
    result.subscribe(() => this.onSaveSuccess(), () => this.onSaveError());
  }

  protected onSaveSuccess() {
    this.isSaving = false;
    this.previousState();
  }

  protected onSaveError() {
    this.isSaving = false;
  }
  protected onError(errorMessage: string) {
    this.jhiAlertService.error(errorMessage, null, null);
  }

  trackVoyageById(index: number, item: IVoyage) {
    return item.id;
  }
}
