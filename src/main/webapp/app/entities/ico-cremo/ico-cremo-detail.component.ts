import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

import { IIcoCremo } from 'app/shared/model/ico-cremo.model';

@Component({
  selector: 'jhi-ico-cremo-detail',
  templateUrl: './ico-cremo-detail.component.html'
})
export class IcoCremoDetailComponent implements OnInit {
  icoCremo: IIcoCremo;

  constructor(protected activatedRoute: ActivatedRoute) {}

  ngOnInit() {
    this.activatedRoute.data.subscribe(({ icoCremo }) => {
      this.icoCremo = icoCremo;
    });
  }

  previousState() {
    window.history.back();
  }
}
