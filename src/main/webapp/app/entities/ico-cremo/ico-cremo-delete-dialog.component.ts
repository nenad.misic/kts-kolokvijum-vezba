import { Component } from '@angular/core';

import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { JhiEventManager } from 'ng-jhipster';

import { IIcoCremo } from 'app/shared/model/ico-cremo.model';
import { IcoCremoService } from './ico-cremo.service';

@Component({
  templateUrl: './ico-cremo-delete-dialog.component.html'
})
export class IcoCremoDeleteDialogComponent {
  icoCremo: IIcoCremo;

  constructor(protected icoCremoService: IcoCremoService, public activeModal: NgbActiveModal, protected eventManager: JhiEventManager) {}

  clear() {
    this.activeModal.dismiss('cancel');
  }

  confirmDelete(id: number) {
    this.icoCremoService.delete(id).subscribe(() => {
      this.eventManager.broadcast({
        name: 'icoCremoListModification',
        content: 'Deleted an icoCremo'
      });
      this.activeModal.dismiss(true);
    });
  }
}
