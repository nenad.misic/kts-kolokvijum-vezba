import { Injectable } from '@angular/core';
import { HttpResponse } from '@angular/common/http';
import { Resolve, ActivatedRouteSnapshot, Routes } from '@angular/router';
import { JhiResolvePagingParams } from 'ng-jhipster';
import { UserRouteAccessService } from 'app/core/auth/user-route-access-service';
import { Observable, of } from 'rxjs';
import { map } from 'rxjs/operators';
import { IcoCremo } from 'app/shared/model/ico-cremo.model';
import { IcoCremoService } from './ico-cremo.service';
import { IcoCremoComponent } from './ico-cremo.component';
import { IcoCremoDetailComponent } from './ico-cremo-detail.component';
import { IcoCremoUpdateComponent } from './ico-cremo-update.component';
import { IIcoCremo } from 'app/shared/model/ico-cremo.model';

@Injectable({ providedIn: 'root' })
export class IcoCremoResolve implements Resolve<IIcoCremo> {
  constructor(private service: IcoCremoService) {}

  resolve(route: ActivatedRouteSnapshot): Observable<IIcoCremo> {
    const id = route.params['id'];
    if (id) {
      return this.service.find(id).pipe(map((icoCremo: HttpResponse<IcoCremo>) => icoCremo.body));
    }
    return of(new IcoCremo());
  }
}

export const icoCremoRoute: Routes = [
  {
    path: '',
    component: IcoCremoComponent,
    resolve: {
      pagingParams: JhiResolvePagingParams
    },
    data: {
      authorities: ['ROLE_USER'],
      defaultSort: 'id,asc',
      pageTitle: 'icyTrainApp.icoCremo.home.title'
    },
    canActivate: [UserRouteAccessService]
  },
  {
    path: ':id/view',
    component: IcoCremoDetailComponent,
    resolve: {
      icoCremo: IcoCremoResolve
    },
    data: {
      authorities: ['ROLE_USER'],
      pageTitle: 'icyTrainApp.icoCremo.home.title'
    },
    canActivate: [UserRouteAccessService]
  },
  {
    path: 'new',
    component: IcoCremoUpdateComponent,
    resolve: {
      icoCremo: IcoCremoResolve
    },
    data: {
      authorities: ['ROLE_USER'],
      pageTitle: 'icyTrainApp.icoCremo.home.title'
    },
    canActivate: [UserRouteAccessService]
  },
  {
    path: ':id/edit',
    component: IcoCremoUpdateComponent,
    resolve: {
      icoCremo: IcoCremoResolve
    },
    data: {
      authorities: ['ROLE_USER'],
      pageTitle: 'icyTrainApp.icoCremo.home.title'
    },
    canActivate: [UserRouteAccessService]
  }
];
