import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';

@NgModule({
  imports: [
    RouterModule.forChild([
      {
        path: 'train',
        loadChildren: () => import('./train/train.module').then(m => m.IcyTrainTrainModule)
      },
      {
        path: 'ico-cremo',
        loadChildren: () => import('./ico-cremo/ico-cremo.module').then(m => m.IcyTrainIcoCremoModule)
      },
      {
        path: 'location',
        loadChildren: () => import('./location/location.module').then(m => m.IcyTrainLocationModule)
      },
      {
        path: 'train-statione',
        loadChildren: () => import('./train-statione/train-statione.module').then(m => m.IcyTrainTrainStationeModule)
      },
      {
        path: 'voyage',
        loadChildren: () => import('./voyage/voyage.module').then(m => m.IcyTrainVoyageModule)
      }
      /* jhipster-needle-add-entity-route - JHipster will add entity modules routes here */
    ])
  ]
})
export class IcyTrainEntityModule {}
