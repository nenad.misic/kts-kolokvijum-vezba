import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

import { ITrainStatione } from 'app/shared/model/train-statione.model';

@Component({
  selector: 'jhi-train-statione-detail',
  templateUrl: './train-statione-detail.component.html'
})
export class TrainStationeDetailComponent implements OnInit {
  trainStatione: ITrainStatione;

  constructor(protected activatedRoute: ActivatedRoute) {}

  ngOnInit() {
    this.activatedRoute.data.subscribe(({ trainStatione }) => {
      this.trainStatione = trainStatione;
    });
  }

  previousState() {
    window.history.back();
  }
}
