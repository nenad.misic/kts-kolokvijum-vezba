import { Component } from '@angular/core';

import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { JhiEventManager } from 'ng-jhipster';

import { ITrainStatione } from 'app/shared/model/train-statione.model';
import { TrainStationeService } from './train-statione.service';

@Component({
  templateUrl: './train-statione-delete-dialog.component.html'
})
export class TrainStationeDeleteDialogComponent {
  trainStatione: ITrainStatione;

  constructor(
    protected trainStationeService: TrainStationeService,
    public activeModal: NgbActiveModal,
    protected eventManager: JhiEventManager
  ) {}

  clear() {
    this.activeModal.dismiss('cancel');
  }

  confirmDelete(id: number) {
    this.trainStationeService.delete(id).subscribe(() => {
      this.eventManager.broadcast({
        name: 'trainStationeListModification',
        content: 'Deleted an trainStatione'
      });
      this.activeModal.dismiss(true);
    });
  }
}
