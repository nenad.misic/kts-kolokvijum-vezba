import { Injectable } from '@angular/core';
import { HttpResponse } from '@angular/common/http';
import { Resolve, ActivatedRouteSnapshot, Routes } from '@angular/router';
import { UserRouteAccessService } from 'app/core/auth/user-route-access-service';
import { Observable, of } from 'rxjs';
import { map } from 'rxjs/operators';
import { TrainStatione } from 'app/shared/model/train-statione.model';
import { TrainStationeService } from './train-statione.service';
import { TrainStationeComponent } from './train-statione.component';
import { TrainStationeDetailComponent } from './train-statione-detail.component';
import { TrainStationeUpdateComponent } from './train-statione-update.component';
import { ITrainStatione } from 'app/shared/model/train-statione.model';

@Injectable({ providedIn: 'root' })
export class TrainStationeResolve implements Resolve<ITrainStatione> {
  constructor(private service: TrainStationeService) {}

  resolve(route: ActivatedRouteSnapshot): Observable<ITrainStatione> {
    const id = route.params['id'];
    if (id) {
      return this.service.find(id).pipe(map((trainStatione: HttpResponse<TrainStatione>) => trainStatione.body));
    }
    return of(new TrainStatione());
  }
}

export const trainStationeRoute: Routes = [
  {
    path: '',
    component: TrainStationeComponent,
    data: {
      authorities: ['ROLE_USER'],
      pageTitle: 'icyTrainApp.trainStatione.home.title'
    },
    canActivate: [UserRouteAccessService]
  },
  {
    path: ':id/view',
    component: TrainStationeDetailComponent,
    resolve: {
      trainStatione: TrainStationeResolve
    },
    data: {
      authorities: ['ROLE_USER'],
      pageTitle: 'icyTrainApp.trainStatione.home.title'
    },
    canActivate: [UserRouteAccessService]
  },
  {
    path: 'new',
    component: TrainStationeUpdateComponent,
    resolve: {
      trainStatione: TrainStationeResolve
    },
    data: {
      authorities: ['ROLE_USER'],
      pageTitle: 'icyTrainApp.trainStatione.home.title'
    },
    canActivate: [UserRouteAccessService]
  },
  {
    path: ':id/edit',
    component: TrainStationeUpdateComponent,
    resolve: {
      trainStatione: TrainStationeResolve
    },
    data: {
      authorities: ['ROLE_USER'],
      pageTitle: 'icyTrainApp.trainStatione.home.title'
    },
    canActivate: [UserRouteAccessService]
  }
];
