import { Injectable } from '@angular/core';
import { HttpClient, HttpResponse } from '@angular/common/http';
import { Observable } from 'rxjs';

import { SERVER_API_URL } from 'app/app.constants';
import { createRequestOption } from 'app/shared/util/request-util';
import { ITrainStatione } from 'app/shared/model/train-statione.model';

type EntityResponseType = HttpResponse<ITrainStatione>;
type EntityArrayResponseType = HttpResponse<ITrainStatione[]>;

@Injectable({ providedIn: 'root' })
export class TrainStationeService {
  public resourceUrl = SERVER_API_URL + 'api/train-stationes';

  constructor(protected http: HttpClient) {}

  create(trainStatione: ITrainStatione): Observable<EntityResponseType> {
    return this.http.post<ITrainStatione>(this.resourceUrl, trainStatione, { observe: 'response' });
  }

  update(trainStatione: ITrainStatione): Observable<EntityResponseType> {
    return this.http.put<ITrainStatione>(this.resourceUrl, trainStatione, { observe: 'response' });
  }

  find(id: number): Observable<EntityResponseType> {
    return this.http.get<ITrainStatione>(`${this.resourceUrl}/${id}`, { observe: 'response' });
  }

  query(req?: any): Observable<EntityArrayResponseType> {
    const options = createRequestOption(req);
    return this.http.get<ITrainStatione[]>(this.resourceUrl, { params: options, observe: 'response' });
  }

  delete(id: number): Observable<HttpResponse<any>> {
    return this.http.delete<any>(`${this.resourceUrl}/${id}`, { observe: 'response' });
  }
}
