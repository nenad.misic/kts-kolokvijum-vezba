import { Component, OnInit } from '@angular/core';
// eslint-disable-next-line @typescript-eslint/no-unused-vars
import { HttpResponse, HttpErrorResponse } from '@angular/common/http';
// eslint-disable-next-line @typescript-eslint/no-unused-vars
import { FormBuilder, Validators } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { Observable } from 'rxjs';
import { JhiAlertService } from 'ng-jhipster';
import { ITrainStatione, TrainStatione } from 'app/shared/model/train-statione.model';
import { TrainStationeService } from './train-statione.service';
import { ILocation } from 'app/shared/model/location.model';
import { LocationService } from 'app/entities/location/location.service';

@Component({
  selector: 'jhi-train-statione-update',
  templateUrl: './train-statione-update.component.html'
})
export class TrainStationeUpdateComponent implements OnInit {
  isSaving: boolean;

  locations: ILocation[];

  editForm = this.fb.group({
    id: [],
    name: [null, [Validators.required]],
    location: []
  });

  constructor(
    protected jhiAlertService: JhiAlertService,
    protected trainStationeService: TrainStationeService,
    protected locationService: LocationService,
    protected activatedRoute: ActivatedRoute,
    private fb: FormBuilder
  ) {}

  ngOnInit() {
    this.isSaving = false;
    this.activatedRoute.data.subscribe(({ trainStatione }) => {
      this.updateForm(trainStatione);
    });
    this.locationService.query({ filter: 'trainstatione-is-null' }).subscribe(
      (res: HttpResponse<ILocation[]>) => {
        if (!this.editForm.get('location').value || !this.editForm.get('location').value.id) {
          this.locations = res.body;
        } else {
          this.locationService
            .find(this.editForm.get('location').value.id)
            .subscribe(
              (subRes: HttpResponse<ILocation>) => (this.locations = [subRes.body].concat(res.body)),
              (subRes: HttpErrorResponse) => this.onError(subRes.message)
            );
        }
      },
      (res: HttpErrorResponse) => this.onError(res.message)
    );
  }

  updateForm(trainStatione: ITrainStatione) {
    this.editForm.patchValue({
      id: trainStatione.id,
      name: trainStatione.name,
      location: trainStatione.location
    });
  }

  previousState() {
    window.history.back();
  }

  save() {
    this.isSaving = true;
    const trainStatione = this.createFromForm();
    if (trainStatione.id !== undefined) {
      this.subscribeToSaveResponse(this.trainStationeService.update(trainStatione));
    } else {
      this.subscribeToSaveResponse(this.trainStationeService.create(trainStatione));
    }
  }

  private createFromForm(): ITrainStatione {
    return {
      ...new TrainStatione(),
      id: this.editForm.get(['id']).value,
      name: this.editForm.get(['name']).value,
      location: this.editForm.get(['location']).value
    };
  }

  protected subscribeToSaveResponse(result: Observable<HttpResponse<ITrainStatione>>) {
    result.subscribe(() => this.onSaveSuccess(), () => this.onSaveError());
  }

  protected onSaveSuccess() {
    this.isSaving = false;
    this.previousState();
  }

  protected onSaveError() {
    this.isSaving = false;
  }
  protected onError(errorMessage: string) {
    this.jhiAlertService.error(errorMessage, null, null);
  }

  trackLocationById(index: number, item: ILocation) {
    return item.id;
  }
}
