import { Component, OnInit, OnDestroy } from '@angular/core';
import { HttpHeaders, HttpResponse } from '@angular/common/http';
import { Subscription } from 'rxjs';
import { JhiEventManager, JhiParseLinks } from 'ng-jhipster';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';

import { ITrainStatione } from 'app/shared/model/train-statione.model';

import { ITEMS_PER_PAGE } from 'app/shared/constants/pagination.constants';
import { TrainStationeService } from './train-statione.service';
import { TrainStationeDeleteDialogComponent } from './train-statione-delete-dialog.component';

@Component({
  selector: 'jhi-train-statione',
  templateUrl: './train-statione.component.html'
})
export class TrainStationeComponent implements OnInit, OnDestroy {
  trainStationes: ITrainStatione[];
  eventSubscriber: Subscription;
  itemsPerPage: number;
  links: any;
  page: any;
  predicate: any;
  reverse: any;
  totalItems: number;

  constructor(
    protected trainStationeService: TrainStationeService,
    protected eventManager: JhiEventManager,
    protected modalService: NgbModal,
    protected parseLinks: JhiParseLinks
  ) {
    this.trainStationes = [];
    this.itemsPerPage = ITEMS_PER_PAGE;
    this.page = 0;
    this.links = {
      last: 0
    };
    this.predicate = 'id';
    this.reverse = true;
  }

  loadAll() {
    this.trainStationeService
      .query({
        page: this.page,
        size: this.itemsPerPage,
        sort: this.sort()
      })
      .subscribe((res: HttpResponse<ITrainStatione[]>) => this.paginateTrainStationes(res.body, res.headers));
  }

  reset() {
    this.page = 0;
    this.trainStationes = [];
    this.loadAll();
  }

  loadPage(page) {
    this.page = page;
    this.loadAll();
  }

  ngOnInit() {
    this.loadAll();
    this.registerChangeInTrainStationes();
  }

  ngOnDestroy() {
    this.eventManager.destroy(this.eventSubscriber);
  }

  trackId(index: number, item: ITrainStatione) {
    return item.id;
  }

  registerChangeInTrainStationes() {
    this.eventSubscriber = this.eventManager.subscribe('trainStationeListModification', () => this.reset());
  }

  delete(trainStatione: ITrainStatione) {
    const modalRef = this.modalService.open(TrainStationeDeleteDialogComponent, { size: 'lg', backdrop: 'static' });
    modalRef.componentInstance.trainStatione = trainStatione;
  }

  sort() {
    const result = [this.predicate + ',' + (this.reverse ? 'asc' : 'desc')];
    if (this.predicate !== 'id') {
      result.push('id');
    }
    return result;
  }

  protected paginateTrainStationes(data: ITrainStatione[], headers: HttpHeaders) {
    this.links = this.parseLinks.parse(headers.get('link'));
    this.totalItems = parseInt(headers.get('X-Total-Count'), 10);
    for (let i = 0; i < data.length; i++) {
      this.trainStationes.push(data[i]);
    }
  }
}
