import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';

import { IcyTrainSharedModule } from 'app/shared/shared.module';
import { TrainStationeComponent } from './train-statione.component';
import { TrainStationeDetailComponent } from './train-statione-detail.component';
import { TrainStationeUpdateComponent } from './train-statione-update.component';
import { TrainStationeDeleteDialogComponent } from './train-statione-delete-dialog.component';
import { trainStationeRoute } from './train-statione.route';

@NgModule({
  imports: [IcyTrainSharedModule, RouterModule.forChild(trainStationeRoute)],
  declarations: [TrainStationeComponent, TrainStationeDetailComponent, TrainStationeUpdateComponent, TrainStationeDeleteDialogComponent],
  entryComponents: [TrainStationeDeleteDialogComponent]
})
export class IcyTrainTrainStationeModule {}
