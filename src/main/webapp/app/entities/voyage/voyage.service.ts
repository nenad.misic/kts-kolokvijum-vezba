import { Injectable } from '@angular/core';
import { HttpClient, HttpResponse } from '@angular/common/http';
import { Observable } from 'rxjs';
import * as moment from 'moment';
// eslint-disable-next-line @typescript-eslint/no-unused-vars
import { DATE_FORMAT } from 'app/shared/constants/input.constants';
import { map } from 'rxjs/operators';

import { SERVER_API_URL } from 'app/app.constants';
import { createRequestOption } from 'app/shared/util/request-util';
import { IVoyage } from 'app/shared/model/voyage.model';

type EntityResponseType = HttpResponse<IVoyage>;
type EntityArrayResponseType = HttpResponse<IVoyage[]>;

@Injectable({ providedIn: 'root' })
export class VoyageService {
  public resourceUrl = SERVER_API_URL + 'api/voyages';

  constructor(protected http: HttpClient) {}

  create(voyage: IVoyage): Observable<EntityResponseType> {
    const copy = this.convertDateFromClient(voyage);
    return this.http
      .post<IVoyage>(this.resourceUrl, copy, { observe: 'response' })
      .pipe(map((res: EntityResponseType) => this.convertDateFromServer(res)));
  }

  update(voyage: IVoyage): Observable<EntityResponseType> {
    const copy = this.convertDateFromClient(voyage);
    return this.http
      .put<IVoyage>(this.resourceUrl, copy, { observe: 'response' })
      .pipe(map((res: EntityResponseType) => this.convertDateFromServer(res)));
  }

  find(id: number): Observable<EntityResponseType> {
    return this.http
      .get<IVoyage>(`${this.resourceUrl}/${id}`, { observe: 'response' })
      .pipe(map((res: EntityResponseType) => this.convertDateFromServer(res)));
  }

  query(req?: any): Observable<EntityArrayResponseType> {
    const options = createRequestOption(req);
    return this.http
      .get<IVoyage[]>(this.resourceUrl, { params: options, observe: 'response' })
      .pipe(map((res: EntityArrayResponseType) => this.convertDateArrayFromServer(res)));
  }

  delete(id: number): Observable<HttpResponse<any>> {
    return this.http.delete<any>(`${this.resourceUrl}/${id}`, { observe: 'response' });
  }

  protected convertDateFromClient(voyage: IVoyage): IVoyage {
    const copy: IVoyage = Object.assign({}, voyage, {
      startDate: voyage.startDate != null && voyage.startDate.isValid() ? voyage.startDate.toJSON() : null,
      endDate: voyage.endDate != null && voyage.endDate.isValid() ? voyage.endDate.toJSON() : null
    });
    return copy;
  }

  protected convertDateFromServer(res: EntityResponseType): EntityResponseType {
    if (res.body) {
      res.body.startDate = res.body.startDate != null ? moment(res.body.startDate) : null;
      res.body.endDate = res.body.endDate != null ? moment(res.body.endDate) : null;
    }
    return res;
  }

  protected convertDateArrayFromServer(res: EntityArrayResponseType): EntityArrayResponseType {
    if (res.body) {
      res.body.forEach((voyage: IVoyage) => {
        voyage.startDate = voyage.startDate != null ? moment(voyage.startDate) : null;
        voyage.endDate = voyage.endDate != null ? moment(voyage.endDate) : null;
      });
    }
    return res;
  }
}
