import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

import { IVoyage } from 'app/shared/model/voyage.model';

@Component({
  selector: 'jhi-voyage-detail',
  templateUrl: './voyage-detail.component.html'
})
export class VoyageDetailComponent implements OnInit {
  voyage: IVoyage;

  constructor(protected activatedRoute: ActivatedRoute) {}

  ngOnInit() {
    this.activatedRoute.data.subscribe(({ voyage }) => {
      this.voyage = voyage;
    });
  }

  previousState() {
    window.history.back();
  }
}
