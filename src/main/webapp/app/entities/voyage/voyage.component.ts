import { Component, OnInit, OnDestroy } from '@angular/core';
import { HttpResponse } from '@angular/common/http';
import { Subscription } from 'rxjs';
import { JhiEventManager } from 'ng-jhipster';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';

import { IVoyage } from 'app/shared/model/voyage.model';
import { VoyageService } from './voyage.service';
import { VoyageDeleteDialogComponent } from './voyage-delete-dialog.component';

@Component({
  selector: 'jhi-voyage',
  templateUrl: './voyage.component.html'
})
export class VoyageComponent implements OnInit, OnDestroy {
  voyages: IVoyage[];
  eventSubscriber: Subscription;

  constructor(protected voyageService: VoyageService, protected eventManager: JhiEventManager, protected modalService: NgbModal) {}

  loadAll() {
    this.voyageService.query().subscribe((res: HttpResponse<IVoyage[]>) => {
      this.voyages = res.body;
    });
  }

  ngOnInit() {
    this.loadAll();
    this.registerChangeInVoyages();
  }

  ngOnDestroy() {
    this.eventManager.destroy(this.eventSubscriber);
  }

  trackId(index: number, item: IVoyage) {
    return item.id;
  }

  registerChangeInVoyages() {
    this.eventSubscriber = this.eventManager.subscribe('voyageListModification', () => this.loadAll());
  }

  delete(voyage: IVoyage) {
    const modalRef = this.modalService.open(VoyageDeleteDialogComponent, { size: 'lg', backdrop: 'static' });
    modalRef.componentInstance.voyage = voyage;
  }
}
