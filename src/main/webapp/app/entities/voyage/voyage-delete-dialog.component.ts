import { Component } from '@angular/core';

import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { JhiEventManager } from 'ng-jhipster';

import { IVoyage } from 'app/shared/model/voyage.model';
import { VoyageService } from './voyage.service';

@Component({
  templateUrl: './voyage-delete-dialog.component.html'
})
export class VoyageDeleteDialogComponent {
  voyage: IVoyage;

  constructor(protected voyageService: VoyageService, public activeModal: NgbActiveModal, protected eventManager: JhiEventManager) {}

  clear() {
    this.activeModal.dismiss('cancel');
  }

  confirmDelete(id: number) {
    this.voyageService.delete(id).subscribe(() => {
      this.eventManager.broadcast({
        name: 'voyageListModification',
        content: 'Deleted an voyage'
      });
      this.activeModal.dismiss(true);
    });
  }
}
