import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';

import { IcyTrainSharedModule } from 'app/shared/shared.module';
import { VoyageComponent } from './voyage.component';
import { VoyageDetailComponent } from './voyage-detail.component';
import { VoyageUpdateComponent } from './voyage-update.component';
import { VoyageDeleteDialogComponent } from './voyage-delete-dialog.component';
import { voyageRoute } from './voyage.route';

@NgModule({
  imports: [IcyTrainSharedModule, RouterModule.forChild(voyageRoute)],
  declarations: [VoyageComponent, VoyageDetailComponent, VoyageUpdateComponent, VoyageDeleteDialogComponent],
  entryComponents: [VoyageDeleteDialogComponent]
})
export class IcyTrainVoyageModule {}
