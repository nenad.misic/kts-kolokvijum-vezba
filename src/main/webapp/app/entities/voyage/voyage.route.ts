import { Injectable } from '@angular/core';
import { HttpResponse } from '@angular/common/http';
import { Resolve, ActivatedRouteSnapshot, Routes } from '@angular/router';
import { UserRouteAccessService } from 'app/core/auth/user-route-access-service';
import { Observable, of } from 'rxjs';
import { map } from 'rxjs/operators';
import { Voyage } from 'app/shared/model/voyage.model';
import { VoyageService } from './voyage.service';
import { VoyageComponent } from './voyage.component';
import { VoyageDetailComponent } from './voyage-detail.component';
import { VoyageUpdateComponent } from './voyage-update.component';
import { IVoyage } from 'app/shared/model/voyage.model';

@Injectable({ providedIn: 'root' })
export class VoyageResolve implements Resolve<IVoyage> {
  constructor(private service: VoyageService) {}

  resolve(route: ActivatedRouteSnapshot): Observable<IVoyage> {
    const id = route.params['id'];
    if (id) {
      return this.service.find(id).pipe(map((voyage: HttpResponse<Voyage>) => voyage.body));
    }
    return of(new Voyage());
  }
}

export const voyageRoute: Routes = [
  {
    path: '',
    component: VoyageComponent,
    data: {
      authorities: ['ROLE_USER'],
      pageTitle: 'icyTrainApp.voyage.home.title'
    },
    canActivate: [UserRouteAccessService]
  },
  {
    path: ':id/view',
    component: VoyageDetailComponent,
    resolve: {
      voyage: VoyageResolve
    },
    data: {
      authorities: ['ROLE_USER'],
      pageTitle: 'icyTrainApp.voyage.home.title'
    },
    canActivate: [UserRouteAccessService]
  },
  {
    path: 'new',
    component: VoyageUpdateComponent,
    resolve: {
      voyage: VoyageResolve
    },
    data: {
      authorities: ['ROLE_USER'],
      pageTitle: 'icyTrainApp.voyage.home.title'
    },
    canActivate: [UserRouteAccessService]
  },
  {
    path: ':id/edit',
    component: VoyageUpdateComponent,
    resolve: {
      voyage: VoyageResolve
    },
    data: {
      authorities: ['ROLE_USER'],
      pageTitle: 'icyTrainApp.voyage.home.title'
    },
    canActivate: [UserRouteAccessService]
  }
];
