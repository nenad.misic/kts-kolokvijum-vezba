import { Component, OnInit } from '@angular/core';
// eslint-disable-next-line @typescript-eslint/no-unused-vars
import { HttpResponse, HttpErrorResponse } from '@angular/common/http';
// eslint-disable-next-line @typescript-eslint/no-unused-vars
import { FormBuilder, Validators } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { Observable } from 'rxjs';
import * as moment from 'moment';
import { DATE_TIME_FORMAT } from 'app/shared/constants/input.constants';
import { JhiAlertService } from 'ng-jhipster';
import { IVoyage, Voyage } from 'app/shared/model/voyage.model';
import { VoyageService } from './voyage.service';
import { ITrainStatione } from 'app/shared/model/train-statione.model';
import { TrainStationeService } from 'app/entities/train-statione/train-statione.service';

@Component({
  selector: 'jhi-voyage-update',
  templateUrl: './voyage-update.component.html'
})
export class VoyageUpdateComponent implements OnInit {
  isSaving: boolean;

  trainstationes: ITrainStatione[];

  editForm = this.fb.group({
    id: [],
    startDate: [],
    endDate: [],
    from: [],
    to: []
  });

  constructor(
    protected jhiAlertService: JhiAlertService,
    protected voyageService: VoyageService,
    protected trainStationeService: TrainStationeService,
    protected activatedRoute: ActivatedRoute,
    private fb: FormBuilder
  ) {}

  ngOnInit() {
    this.isSaving = false;
    this.activatedRoute.data.subscribe(({ voyage }) => {
      this.updateForm(voyage);
    });
    this.trainStationeService
      .query()
      .subscribe(
        (res: HttpResponse<ITrainStatione[]>) => (this.trainstationes = res.body),
        (res: HttpErrorResponse) => this.onError(res.message)
      );
  }

  updateForm(voyage: IVoyage) {
    this.editForm.patchValue({
      id: voyage.id,
      startDate: voyage.startDate != null ? voyage.startDate.format(DATE_TIME_FORMAT) : null,
      endDate: voyage.endDate != null ? voyage.endDate.format(DATE_TIME_FORMAT) : null,
      from: voyage.from,
      to: voyage.to
    });
  }

  previousState() {
    window.history.back();
  }

  save() {
    this.isSaving = true;
    const voyage = this.createFromForm();
    if (voyage.id !== undefined) {
      this.subscribeToSaveResponse(this.voyageService.update(voyage));
    } else {
      this.subscribeToSaveResponse(this.voyageService.create(voyage));
    }
  }

  private createFromForm(): IVoyage {
    return {
      ...new Voyage(),
      id: this.editForm.get(['id']).value,
      startDate:
        this.editForm.get(['startDate']).value != null ? moment(this.editForm.get(['startDate']).value, DATE_TIME_FORMAT) : undefined,
      endDate: this.editForm.get(['endDate']).value != null ? moment(this.editForm.get(['endDate']).value, DATE_TIME_FORMAT) : undefined,
      from: this.editForm.get(['from']).value,
      to: this.editForm.get(['to']).value
    };
  }

  protected subscribeToSaveResponse(result: Observable<HttpResponse<IVoyage>>) {
    result.subscribe(() => this.onSaveSuccess(), () => this.onSaveError());
  }

  protected onSaveSuccess() {
    this.isSaving = false;
    this.previousState();
  }

  protected onSaveError() {
    this.isSaving = false;
  }
  protected onError(errorMessage: string) {
    this.jhiAlertService.error(errorMessage, null, null);
  }

  trackTrainStationeById(index: number, item: ITrainStatione) {
    return item.id;
  }
}
