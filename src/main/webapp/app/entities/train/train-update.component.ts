import { Component, OnInit } from '@angular/core';
// eslint-disable-next-line @typescript-eslint/no-unused-vars
import { HttpResponse, HttpErrorResponse } from '@angular/common/http';
// eslint-disable-next-line @typescript-eslint/no-unused-vars
import { FormBuilder, Validators } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { Observable } from 'rxjs';
import { JhiAlertService } from 'ng-jhipster';
import { ITrain, Train } from 'app/shared/model/train.model';
import { TrainService } from './train.service';
import { IVoyage } from 'app/shared/model/voyage.model';
import { VoyageService } from 'app/entities/voyage/voyage.service';

@Component({
  selector: 'jhi-train-update',
  templateUrl: './train-update.component.html'
})
export class TrainUpdateComponent implements OnInit {
  isSaving: boolean;

  voyages: IVoyage[];

  editForm = this.fb.group({
    id: [],
    name: [null, [Validators.required]],
    numOfCars: [],
    voyage: []
  });

  constructor(
    protected jhiAlertService: JhiAlertService,
    protected trainService: TrainService,
    protected voyageService: VoyageService,
    protected activatedRoute: ActivatedRoute,
    private fb: FormBuilder
  ) {}

  ngOnInit() {
    this.isSaving = false;
    this.activatedRoute.data.subscribe(({ train }) => {
      this.updateForm(train);
    });
    this.voyageService
      .query()
      .subscribe((res: HttpResponse<IVoyage[]>) => (this.voyages = res.body), (res: HttpErrorResponse) => this.onError(res.message));
  }

  updateForm(train: ITrain) {
    this.editForm.patchValue({
      id: train.id,
      name: train.name,
      numOfCars: train.numOfCars,
      voyage: train.voyage
    });
  }

  previousState() {
    window.history.back();
  }

  save() {
    this.isSaving = true;
    const train = this.createFromForm();
    if (train.id !== undefined) {
      this.subscribeToSaveResponse(this.trainService.update(train));
    } else {
      this.subscribeToSaveResponse(this.trainService.create(train));
    }
  }

  private createFromForm(): ITrain {
    return {
      ...new Train(),
      id: this.editForm.get(['id']).value,
      name: this.editForm.get(['name']).value,
      numOfCars: this.editForm.get(['numOfCars']).value,
      voyage: this.editForm.get(['voyage']).value
    };
  }

  protected subscribeToSaveResponse(result: Observable<HttpResponse<ITrain>>) {
    result.subscribe(() => this.onSaveSuccess(), () => this.onSaveError());
  }

  protected onSaveSuccess() {
    this.isSaving = false;
    this.previousState();
  }

  protected onSaveError() {
    this.isSaving = false;
  }
  protected onError(errorMessage: string) {
    this.jhiAlertService.error(errorMessage, null, null);
  }

  trackVoyageById(index: number, item: IVoyage) {
    return item.id;
  }
}
