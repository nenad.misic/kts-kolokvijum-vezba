import { Injectable } from '@angular/core';
import { HttpResponse } from '@angular/common/http';
import { Resolve, ActivatedRouteSnapshot, Routes } from '@angular/router';
import { UserRouteAccessService } from 'app/core/auth/user-route-access-service';
import { Observable, of } from 'rxjs';
import { map } from 'rxjs/operators';
import { Train } from 'app/shared/model/train.model';
import { TrainService } from './train.service';
import { TrainComponent } from './train.component';
import { TrainDetailComponent } from './train-detail.component';
import { TrainUpdateComponent } from './train-update.component';
import { ITrain } from 'app/shared/model/train.model';

@Injectable({ providedIn: 'root' })
export class TrainResolve implements Resolve<ITrain> {
  constructor(private service: TrainService) {}

  resolve(route: ActivatedRouteSnapshot): Observable<ITrain> {
    const id = route.params['id'];
    if (id) {
      return this.service.find(id).pipe(map((train: HttpResponse<Train>) => train.body));
    }
    return of(new Train());
  }
}

export const trainRoute: Routes = [
  {
    path: '',
    component: TrainComponent,
    data: {
      authorities: ['ROLE_USER'],
      pageTitle: 'icyTrainApp.train.home.title'
    },
    canActivate: [UserRouteAccessService]
  },
  {
    path: ':id/view',
    component: TrainDetailComponent,
    resolve: {
      train: TrainResolve
    },
    data: {
      authorities: ['ROLE_USER'],
      pageTitle: 'icyTrainApp.train.home.title'
    },
    canActivate: [UserRouteAccessService]
  },
  {
    path: 'new',
    component: TrainUpdateComponent,
    resolve: {
      train: TrainResolve
    },
    data: {
      authorities: ['ROLE_USER'],
      pageTitle: 'icyTrainApp.train.home.title'
    },
    canActivate: [UserRouteAccessService]
  },
  {
    path: ':id/edit',
    component: TrainUpdateComponent,
    resolve: {
      train: TrainResolve
    },
    data: {
      authorities: ['ROLE_USER'],
      pageTitle: 'icyTrainApp.train.home.title'
    },
    canActivate: [UserRouteAccessService]
  }
];
