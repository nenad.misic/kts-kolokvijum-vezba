import { IVoyage } from 'app/shared/model/voyage.model';

export interface ITrain {
  id?: number;
  name?: string;
  numOfCars?: number;
  voyage?: IVoyage;
}

export class Train implements ITrain {
  constructor(public id?: number, public name?: string, public numOfCars?: number, public voyage?: IVoyage) {}
}
