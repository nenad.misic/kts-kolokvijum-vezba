import { ILocation } from 'app/shared/model/location.model';

export interface ITrainStatione {
  id?: number;
  name?: string;
  location?: ILocation;
}

export class TrainStatione implements ITrainStatione {
  constructor(public id?: number, public name?: string, public location?: ILocation) {}
}
