import { IVoyage } from 'app/shared/model/voyage.model';

export interface IIcoCremo {
  id?: number;
  name?: string;
  numOfKugeln?: number;
  keksKornetKeks?: boolean;
  price?: number;
  voyage?: IVoyage;
}

export class IcoCremo implements IIcoCremo {
  constructor(
    public id?: number,
    public name?: string,
    public numOfKugeln?: number,
    public keksKornetKeks?: boolean,
    public price?: number,
    public voyage?: IVoyage
  ) {
    this.keksKornetKeks = this.keksKornetKeks || false;
  }
}
