import { Moment } from 'moment';
import { IIcoCremo } from 'app/shared/model/ico-cremo.model';
import { ITrain } from 'app/shared/model/train.model';
import { ITrainStatione } from 'app/shared/model/train-statione.model';

export interface IVoyage {
  id?: number;
  startDate?: Moment;
  endDate?: Moment;
  icecremos?: IIcoCremo[];
  trains?: ITrain[];
  from?: ITrainStatione;
  to?: ITrainStatione;
}

export class Voyage implements IVoyage {
  constructor(
    public id?: number,
    public startDate?: Moment,
    public endDate?: Moment,
    public icecremos?: IIcoCremo[],
    public trains?: ITrain[],
    public from?: ITrainStatione,
    public to?: ITrainStatione
  ) {}
}
