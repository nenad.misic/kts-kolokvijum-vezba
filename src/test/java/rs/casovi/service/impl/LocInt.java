package rs.casovi.service.impl;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;
import rs.casovi.domain.Location;

import java.util.List;

import static org.junit.Assert.assertEquals;

@RunWith(SpringRunner.class)
@SpringBootTest()
public class LocInt {
    @Autowired
    private LocationServiceImpl locationService;


    @Test
    public void whenValidId_thenLocationShouldBeFound() {
        Long id = 100L;
        List<Location> s = locationService.findAll();
        Location found = locationService.findOne(id).get();

        assertEquals("Shit", found.getCity());
    }
}
