package rs.casovi.service.impl;

import static org.junit.Assert.assertEquals;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.context.SpringBootTest.WebEnvironment;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.boot.web.server.LocalServerPort;
import org.springframework.test.context.TestPropertySource;
import org.springframework.test.context.jdbc.Sql;
import org.springframework.test.context.junit4.SpringRunner;
import rs.casovi.domain.Location;
import rs.casovi.repository.LocationRepository;

import java.util.Optional;

@RunWith(SpringRunner.class)
@SpringBootTest(webEnvironment=WebEnvironment.RANDOM_PORT)
public class LocationServiceImplTest {

    @Autowired
    private LocationServiceImpl locationService;

    @LocalServerPort
    private int port;

    @MockBean
    private LocationRepository locationRepositoryMockered;

    @Before
    public void setUp(){
        Location l = new Location();
        l.setId(100L);
        l.setCity("Shit");
        l.setPostalCode("100 000");
        l.setStateProvince("ShitShit20");
        l.setStreetAddress("Boulevard 1000");

        Mockito.when(locationRepositoryMockered.findById(l.getId())).thenReturn(Optional.of(l));
    }

    @Test
    public void whenValidId_thenLocationShouldBeFound() {
        Long id = 100L;
        Location found = locationService.findOne(id).get();

        assertEquals("Shit", found.getCity());
    }

    @After
    public void port() {
        System.out.println("Port was: " + port);
    }
}
