package rs.casovi.web.rest;

import rs.casovi.IcyTrainApp;
import rs.casovi.domain.IcoCremo;
import rs.casovi.repository.IcoCremoRepository;
import rs.casovi.service.IcoCremoService;
import rs.casovi.web.rest.errors.ExceptionTranslator;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.web.PageableHandlerMethodArgumentResolver;
import org.springframework.http.MediaType;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.validation.Validator;

import javax.persistence.EntityManager;
import java.util.List;

import static rs.casovi.web.rest.TestUtil.createFormattingConversionService;
import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

/**
 * Integration tests for the {@link IcoCremoResource} REST controller.
 */
@SpringBootTest(classes = IcyTrainApp.class)
public class IcoCremoResourceIT {

    private static final String DEFAULT_NAME = "AAAAAAAAAA";
    private static final String UPDATED_NAME = "BBBBBBBBBB";

    private static final Integer DEFAULT_NUM_OF_KUGELN = 1;
    private static final Integer UPDATED_NUM_OF_KUGELN = 2;

    private static final Boolean DEFAULT_KEKS_KORNET_KEKS = false;
    private static final Boolean UPDATED_KEKS_KORNET_KEKS = true;

    private static final Float DEFAULT_PRICE = 1F;
    private static final Float UPDATED_PRICE = 2F;

    @Autowired
    private IcoCremoRepository icoCremoRepository;

    @Autowired
    private IcoCremoService icoCremoService;

    @Autowired
    private MappingJackson2HttpMessageConverter jacksonMessageConverter;

    @Autowired
    private PageableHandlerMethodArgumentResolver pageableArgumentResolver;

    @Autowired
    private ExceptionTranslator exceptionTranslator;

    @Autowired
    private EntityManager em;

    @Autowired
    private Validator validator;

    private MockMvc restIcoCremoMockMvc;

    private IcoCremo icoCremo;

    @BeforeEach
    public void setup() {
        MockitoAnnotations.initMocks(this);
        final IcoCremoResource icoCremoResource = new IcoCremoResource(icoCremoService);
        this.restIcoCremoMockMvc = MockMvcBuilders.standaloneSetup(icoCremoResource)
            .setCustomArgumentResolvers(pageableArgumentResolver)
            .setControllerAdvice(exceptionTranslator)
            .setConversionService(createFormattingConversionService())
            .setMessageConverters(jacksonMessageConverter)
            .setValidator(validator).build();
    }

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static IcoCremo createEntity(EntityManager em) {
        IcoCremo icoCremo = new IcoCremo()
            .name(DEFAULT_NAME)
            .numOfKugeln(DEFAULT_NUM_OF_KUGELN)
            .keksKornetKeks(DEFAULT_KEKS_KORNET_KEKS)
            .price(DEFAULT_PRICE);
        return icoCremo;
    }
    /**
     * Create an updated entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static IcoCremo createUpdatedEntity(EntityManager em) {
        IcoCremo icoCremo = new IcoCremo()
            .name(UPDATED_NAME)
            .numOfKugeln(UPDATED_NUM_OF_KUGELN)
            .keksKornetKeks(UPDATED_KEKS_KORNET_KEKS)
            .price(UPDATED_PRICE);
        return icoCremo;
    }

    @BeforeEach
    public void initTest() {
        icoCremo = createEntity(em);
    }

    @Test
    @Transactional
    public void createIcoCremo() throws Exception {
        int databaseSizeBeforeCreate = icoCremoRepository.findAll().size();

        // Create the IcoCremo
        restIcoCremoMockMvc.perform(post("/api/ico-cremos")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(icoCremo)))
            .andExpect(status().isCreated());

        // Validate the IcoCremo in the database
        List<IcoCremo> icoCremoList = icoCremoRepository.findAll();
        assertThat(icoCremoList).hasSize(databaseSizeBeforeCreate + 1);
        IcoCremo testIcoCremo = icoCremoList.get(icoCremoList.size() - 1);
        assertThat(testIcoCremo.getName()).isEqualTo(DEFAULT_NAME);
        assertThat(testIcoCremo.getNumOfKugeln()).isEqualTo(DEFAULT_NUM_OF_KUGELN);
        assertThat(testIcoCremo.isKeksKornetKeks()).isEqualTo(DEFAULT_KEKS_KORNET_KEKS);
        assertThat(testIcoCremo.getPrice()).isEqualTo(DEFAULT_PRICE);
    }

    @Test
    @Transactional
    public void createIcoCremoWithExistingId() throws Exception {
        int databaseSizeBeforeCreate = icoCremoRepository.findAll().size();

        // Create the IcoCremo with an existing ID
        icoCremo.setId(1L);

        // An entity with an existing ID cannot be created, so this API call must fail
        restIcoCremoMockMvc.perform(post("/api/ico-cremos")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(icoCremo)))
            .andExpect(status().isBadRequest());

        // Validate the IcoCremo in the database
        List<IcoCremo> icoCremoList = icoCremoRepository.findAll();
        assertThat(icoCremoList).hasSize(databaseSizeBeforeCreate);
    }


    @Test
    @Transactional
    public void getAllIcoCremos() throws Exception {
        // Initialize the database
        icoCremoRepository.saveAndFlush(icoCremo);

        // Get all the icoCremoList
        restIcoCremoMockMvc.perform(get("/api/ico-cremos?sort=id,desc"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(icoCremo.getId().intValue())))
            .andExpect(jsonPath("$.[*].name").value(hasItem(DEFAULT_NAME)))
            .andExpect(jsonPath("$.[*].numOfKugeln").value(hasItem(DEFAULT_NUM_OF_KUGELN)))
            .andExpect(jsonPath("$.[*].keksKornetKeks").value(hasItem(DEFAULT_KEKS_KORNET_KEKS.booleanValue())))
            .andExpect(jsonPath("$.[*].price").value(hasItem(DEFAULT_PRICE.doubleValue())));
    }
    
    @Test
    @Transactional
    public void getIcoCremo() throws Exception {
        // Initialize the database
        icoCremoRepository.saveAndFlush(icoCremo);

        // Get the icoCremo
        restIcoCremoMockMvc.perform(get("/api/ico-cremos/{id}", icoCremo.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.id").value(icoCremo.getId().intValue()))
            .andExpect(jsonPath("$.name").value(DEFAULT_NAME))
            .andExpect(jsonPath("$.numOfKugeln").value(DEFAULT_NUM_OF_KUGELN))
            .andExpect(jsonPath("$.keksKornetKeks").value(DEFAULT_KEKS_KORNET_KEKS.booleanValue()))
            .andExpect(jsonPath("$.price").value(DEFAULT_PRICE.doubleValue()));
    }

    @Test
    @Transactional
    public void getNonExistingIcoCremo() throws Exception {
        // Get the icoCremo
        restIcoCremoMockMvc.perform(get("/api/ico-cremos/{id}", Long.MAX_VALUE))
            .andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    public void updateIcoCremo() throws Exception {
        // Initialize the database
        icoCremoService.save(icoCremo);

        int databaseSizeBeforeUpdate = icoCremoRepository.findAll().size();

        // Update the icoCremo
        IcoCremo updatedIcoCremo = icoCremoRepository.findById(icoCremo.getId()).get();
        // Disconnect from session so that the updates on updatedIcoCremo are not directly saved in db
        em.detach(updatedIcoCremo);
        updatedIcoCremo
            .name(UPDATED_NAME)
            .numOfKugeln(UPDATED_NUM_OF_KUGELN)
            .keksKornetKeks(UPDATED_KEKS_KORNET_KEKS)
            .price(UPDATED_PRICE);

        restIcoCremoMockMvc.perform(put("/api/ico-cremos")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(updatedIcoCremo)))
            .andExpect(status().isOk());

        // Validate the IcoCremo in the database
        List<IcoCremo> icoCremoList = icoCremoRepository.findAll();
        assertThat(icoCremoList).hasSize(databaseSizeBeforeUpdate);
        IcoCremo testIcoCremo = icoCremoList.get(icoCremoList.size() - 1);
        assertThat(testIcoCremo.getName()).isEqualTo(UPDATED_NAME);
        assertThat(testIcoCremo.getNumOfKugeln()).isEqualTo(UPDATED_NUM_OF_KUGELN);
        assertThat(testIcoCremo.isKeksKornetKeks()).isEqualTo(UPDATED_KEKS_KORNET_KEKS);
        assertThat(testIcoCremo.getPrice()).isEqualTo(UPDATED_PRICE);
    }

    @Test
    @Transactional
    public void updateNonExistingIcoCremo() throws Exception {
        int databaseSizeBeforeUpdate = icoCremoRepository.findAll().size();

        // Create the IcoCremo

        // If the entity doesn't have an ID, it will throw BadRequestAlertException
        restIcoCremoMockMvc.perform(put("/api/ico-cremos")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(icoCremo)))
            .andExpect(status().isBadRequest());

        // Validate the IcoCremo in the database
        List<IcoCremo> icoCremoList = icoCremoRepository.findAll();
        assertThat(icoCremoList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    public void deleteIcoCremo() throws Exception {
        // Initialize the database
        icoCremoService.save(icoCremo);

        int databaseSizeBeforeDelete = icoCremoRepository.findAll().size();

        // Delete the icoCremo
        restIcoCremoMockMvc.perform(delete("/api/ico-cremos/{id}", icoCremo.getId())
            .accept(TestUtil.APPLICATION_JSON_UTF8))
            .andExpect(status().isNoContent());

        // Validate the database contains one less item
        List<IcoCremo> icoCremoList = icoCremoRepository.findAll();
        assertThat(icoCremoList).hasSize(databaseSizeBeforeDelete - 1);
    }
}
