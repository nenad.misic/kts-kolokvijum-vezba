package rs.casovi.web.rest;

import rs.casovi.IcyTrainApp;
import rs.casovi.domain.TrainStatione;
import rs.casovi.repository.TrainStationeRepository;
import rs.casovi.service.TrainStationeService;
import rs.casovi.web.rest.errors.ExceptionTranslator;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.web.PageableHandlerMethodArgumentResolver;
import org.springframework.http.MediaType;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.validation.Validator;

import javax.persistence.EntityManager;
import java.util.List;

import static rs.casovi.web.rest.TestUtil.createFormattingConversionService;
import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

/**
 * Integration tests for the {@link TrainStationeResource} REST controller.
 */
@SpringBootTest(classes = IcyTrainApp.class)
public class TrainStationeResourceIT {

    private static final String DEFAULT_NAME = "AAAAAAAAAA";
    private static final String UPDATED_NAME = "BBBBBBBBBB";

    @Autowired
    private TrainStationeRepository trainStationeRepository;

    @Autowired
    private TrainStationeService trainStationeService;

    @Autowired
    private MappingJackson2HttpMessageConverter jacksonMessageConverter;

    @Autowired
    private PageableHandlerMethodArgumentResolver pageableArgumentResolver;

    @Autowired
    private ExceptionTranslator exceptionTranslator;

    @Autowired
    private EntityManager em;

    @Autowired
    private Validator validator;

    private MockMvc restTrainStationeMockMvc;

    private TrainStatione trainStatione;

    @BeforeEach
    public void setup() {
        MockitoAnnotations.initMocks(this);
        final TrainStationeResource trainStationeResource = new TrainStationeResource(trainStationeService);
        this.restTrainStationeMockMvc = MockMvcBuilders.standaloneSetup(trainStationeResource)
            .setCustomArgumentResolvers(pageableArgumentResolver)
            .setControllerAdvice(exceptionTranslator)
            .setConversionService(createFormattingConversionService())
            .setMessageConverters(jacksonMessageConverter)
            .setValidator(validator).build();
    }

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static TrainStatione createEntity(EntityManager em) {
        TrainStatione trainStatione = new TrainStatione()
            .name(DEFAULT_NAME);
        return trainStatione;
    }
    /**
     * Create an updated entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static TrainStatione createUpdatedEntity(EntityManager em) {
        TrainStatione trainStatione = new TrainStatione()
            .name(UPDATED_NAME);
        return trainStatione;
    }

    @BeforeEach
    public void initTest() {
        trainStatione = createEntity(em);
    }

    @Test
    @Transactional
    public void createTrainStatione() throws Exception {
        int databaseSizeBeforeCreate = trainStationeRepository.findAll().size();

        // Create the TrainStatione
        restTrainStationeMockMvc.perform(post("/api/train-stationes")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(trainStatione)))
            .andExpect(status().isCreated());

        // Validate the TrainStatione in the database
        List<TrainStatione> trainStationeList = trainStationeRepository.findAll();
        assertThat(trainStationeList).hasSize(databaseSizeBeforeCreate + 1);
        TrainStatione testTrainStatione = trainStationeList.get(trainStationeList.size() - 1);
        assertThat(testTrainStatione.getName()).isEqualTo(DEFAULT_NAME);
    }

    @Test
    @Transactional
    public void createTrainStationeWithExistingId() throws Exception {
        int databaseSizeBeforeCreate = trainStationeRepository.findAll().size();

        // Create the TrainStatione with an existing ID
        trainStatione.setId(1L);

        // An entity with an existing ID cannot be created, so this API call must fail
        restTrainStationeMockMvc.perform(post("/api/train-stationes")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(trainStatione)))
            .andExpect(status().isBadRequest());

        // Validate the TrainStatione in the database
        List<TrainStatione> trainStationeList = trainStationeRepository.findAll();
        assertThat(trainStationeList).hasSize(databaseSizeBeforeCreate);
    }


    @Test
    @Transactional
    public void checkNameIsRequired() throws Exception {
        int databaseSizeBeforeTest = trainStationeRepository.findAll().size();
        // set the field null
        trainStatione.setName(null);

        // Create the TrainStatione, which fails.

        restTrainStationeMockMvc.perform(post("/api/train-stationes")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(trainStatione)))
            .andExpect(status().isBadRequest());

        List<TrainStatione> trainStationeList = trainStationeRepository.findAll();
        assertThat(trainStationeList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void getAllTrainStationes() throws Exception {
        // Initialize the database
        trainStationeRepository.saveAndFlush(trainStatione);

        // Get all the trainStationeList
        restTrainStationeMockMvc.perform(get("/api/train-stationes?sort=id,desc"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(trainStatione.getId().intValue())))
            .andExpect(jsonPath("$.[*].name").value(hasItem(DEFAULT_NAME)));
    }
    
    @Test
    @Transactional
    public void getTrainStatione() throws Exception {
        // Initialize the database
        trainStationeRepository.saveAndFlush(trainStatione);

        // Get the trainStatione
        restTrainStationeMockMvc.perform(get("/api/train-stationes/{id}", trainStatione.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.id").value(trainStatione.getId().intValue()))
            .andExpect(jsonPath("$.name").value(DEFAULT_NAME));
    }

    @Test
    @Transactional
    public void getNonExistingTrainStatione() throws Exception {
        // Get the trainStatione
        restTrainStationeMockMvc.perform(get("/api/train-stationes/{id}", Long.MAX_VALUE))
            .andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    public void updateTrainStatione() throws Exception {
        // Initialize the database
        trainStationeService.save(trainStatione);

        int databaseSizeBeforeUpdate = trainStationeRepository.findAll().size();

        // Update the trainStatione
        TrainStatione updatedTrainStatione = trainStationeRepository.findById(trainStatione.getId()).get();
        // Disconnect from session so that the updates on updatedTrainStatione are not directly saved in db
        em.detach(updatedTrainStatione);
        updatedTrainStatione
            .name(UPDATED_NAME);

        restTrainStationeMockMvc.perform(put("/api/train-stationes")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(updatedTrainStatione)))
            .andExpect(status().isOk());

        // Validate the TrainStatione in the database
        List<TrainStatione> trainStationeList = trainStationeRepository.findAll();
        assertThat(trainStationeList).hasSize(databaseSizeBeforeUpdate);
        TrainStatione testTrainStatione = trainStationeList.get(trainStationeList.size() - 1);
        assertThat(testTrainStatione.getName()).isEqualTo(UPDATED_NAME);
    }

    @Test
    @Transactional
    public void updateNonExistingTrainStatione() throws Exception {
        int databaseSizeBeforeUpdate = trainStationeRepository.findAll().size();

        // Create the TrainStatione

        // If the entity doesn't have an ID, it will throw BadRequestAlertException
        restTrainStationeMockMvc.perform(put("/api/train-stationes")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(trainStatione)))
            .andExpect(status().isBadRequest());

        // Validate the TrainStatione in the database
        List<TrainStatione> trainStationeList = trainStationeRepository.findAll();
        assertThat(trainStationeList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    public void deleteTrainStatione() throws Exception {
        // Initialize the database
        trainStationeService.save(trainStatione);

        int databaseSizeBeforeDelete = trainStationeRepository.findAll().size();

        // Delete the trainStatione
        restTrainStationeMockMvc.perform(delete("/api/train-stationes/{id}", trainStatione.getId())
            .accept(TestUtil.APPLICATION_JSON_UTF8))
            .andExpect(status().isNoContent());

        // Validate the database contains one less item
        List<TrainStatione> trainStationeList = trainStationeRepository.findAll();
        assertThat(trainStationeList).hasSize(databaseSizeBeforeDelete - 1);
    }
}
