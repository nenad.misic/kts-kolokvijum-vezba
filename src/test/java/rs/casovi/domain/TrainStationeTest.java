package rs.casovi.domain;

import org.junit.jupiter.api.Test;
import static org.assertj.core.api.Assertions.assertThat;
import rs.casovi.web.rest.TestUtil;

public class TrainStationeTest {

    @Test
    public void equalsVerifier() throws Exception {
        TestUtil.equalsVerifier(TrainStatione.class);
        TrainStatione trainStatione1 = new TrainStatione();
        trainStatione1.setId(1L);
        TrainStatione trainStatione2 = new TrainStatione();
        trainStatione2.setId(trainStatione1.getId());
        assertThat(trainStatione1).isEqualTo(trainStatione2);
        trainStatione2.setId(2L);
        assertThat(trainStatione1).isNotEqualTo(trainStatione2);
        trainStatione1.setId(null);
        assertThat(trainStatione1).isNotEqualTo(trainStatione2);
    }
}
