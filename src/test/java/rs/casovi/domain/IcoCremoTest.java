package rs.casovi.domain;

import org.junit.jupiter.api.Test;
import static org.assertj.core.api.Assertions.assertThat;
import rs.casovi.web.rest.TestUtil;

public class IcoCremoTest {

    @Test
    public void equalsVerifier() throws Exception {
        TestUtil.equalsVerifier(IcoCremo.class);
        IcoCremo icoCremo1 = new IcoCremo();
        icoCremo1.setId(1L);
        IcoCremo icoCremo2 = new IcoCremo();
        icoCremo2.setId(icoCremo1.getId());
        assertThat(icoCremo1).isEqualTo(icoCremo2);
        icoCremo2.setId(2L);
        assertThat(icoCremo1).isNotEqualTo(icoCremo2);
        icoCremo1.setId(null);
        assertThat(icoCremo1).isNotEqualTo(icoCremo2);
    }
}
