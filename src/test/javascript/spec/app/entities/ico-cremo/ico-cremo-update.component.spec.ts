import { ComponentFixture, TestBed, fakeAsync, tick } from '@angular/core/testing';
import { HttpResponse } from '@angular/common/http';
import { FormBuilder } from '@angular/forms';
import { of } from 'rxjs';

import { IcyTrainTestModule } from '../../../test.module';
import { IcoCremoUpdateComponent } from 'app/entities/ico-cremo/ico-cremo-update.component';
import { IcoCremoService } from 'app/entities/ico-cremo/ico-cremo.service';
import { IcoCremo } from 'app/shared/model/ico-cremo.model';

describe('Component Tests', () => {
  describe('IcoCremo Management Update Component', () => {
    let comp: IcoCremoUpdateComponent;
    let fixture: ComponentFixture<IcoCremoUpdateComponent>;
    let service: IcoCremoService;

    beforeEach(() => {
      TestBed.configureTestingModule({
        imports: [IcyTrainTestModule],
        declarations: [IcoCremoUpdateComponent],
        providers: [FormBuilder]
      })
        .overrideTemplate(IcoCremoUpdateComponent, '')
        .compileComponents();

      fixture = TestBed.createComponent(IcoCremoUpdateComponent);
      comp = fixture.componentInstance;
      service = fixture.debugElement.injector.get(IcoCremoService);
    });

    describe('save', () => {
      it('Should call update service on save for existing entity', fakeAsync(() => {
        // GIVEN
        const entity = new IcoCremo(123);
        spyOn(service, 'update').and.returnValue(of(new HttpResponse({ body: entity })));
        comp.updateForm(entity);
        // WHEN
        comp.save();
        tick(); // simulate async

        // THEN
        expect(service.update).toHaveBeenCalledWith(entity);
        expect(comp.isSaving).toEqual(false);
      }));

      it('Should call create service on save for new entity', fakeAsync(() => {
        // GIVEN
        const entity = new IcoCremo();
        spyOn(service, 'create').and.returnValue(of(new HttpResponse({ body: entity })));
        comp.updateForm(entity);
        // WHEN
        comp.save();
        tick(); // simulate async

        // THEN
        expect(service.create).toHaveBeenCalledWith(entity);
        expect(comp.isSaving).toEqual(false);
      }));
    });
  });
});
