import { ComponentFixture, TestBed } from '@angular/core/testing';
import { ActivatedRoute } from '@angular/router';
import { of } from 'rxjs';

import { IcyTrainTestModule } from '../../../test.module';
import { IcoCremoDetailComponent } from 'app/entities/ico-cremo/ico-cremo-detail.component';
import { IcoCremo } from 'app/shared/model/ico-cremo.model';

describe('Component Tests', () => {
  describe('IcoCremo Management Detail Component', () => {
    let comp: IcoCremoDetailComponent;
    let fixture: ComponentFixture<IcoCremoDetailComponent>;
    const route = ({ data: of({ icoCremo: new IcoCremo(123) }) } as any) as ActivatedRoute;

    beforeEach(() => {
      TestBed.configureTestingModule({
        imports: [IcyTrainTestModule],
        declarations: [IcoCremoDetailComponent],
        providers: [{ provide: ActivatedRoute, useValue: route }]
      })
        .overrideTemplate(IcoCremoDetailComponent, '')
        .compileComponents();
      fixture = TestBed.createComponent(IcoCremoDetailComponent);
      comp = fixture.componentInstance;
    });

    describe('OnInit', () => {
      it('Should call load all on init', () => {
        // GIVEN

        // WHEN
        comp.ngOnInit();

        // THEN
        expect(comp.icoCremo).toEqual(jasmine.objectContaining({ id: 123 }));
      });
    });
  });
});
