import { ComponentFixture, TestBed, inject, fakeAsync, tick } from '@angular/core/testing';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { of } from 'rxjs';
import { JhiEventManager } from 'ng-jhipster';

import { IcyTrainTestModule } from '../../../test.module';
import { IcoCremoDeleteDialogComponent } from 'app/entities/ico-cremo/ico-cremo-delete-dialog.component';
import { IcoCremoService } from 'app/entities/ico-cremo/ico-cremo.service';

describe('Component Tests', () => {
  describe('IcoCremo Management Delete Component', () => {
    let comp: IcoCremoDeleteDialogComponent;
    let fixture: ComponentFixture<IcoCremoDeleteDialogComponent>;
    let service: IcoCremoService;
    let mockEventManager: any;
    let mockActiveModal: any;

    beforeEach(() => {
      TestBed.configureTestingModule({
        imports: [IcyTrainTestModule],
        declarations: [IcoCremoDeleteDialogComponent]
      })
        .overrideTemplate(IcoCremoDeleteDialogComponent, '')
        .compileComponents();
      fixture = TestBed.createComponent(IcoCremoDeleteDialogComponent);
      comp = fixture.componentInstance;
      service = fixture.debugElement.injector.get(IcoCremoService);
      mockEventManager = fixture.debugElement.injector.get(JhiEventManager);
      mockActiveModal = fixture.debugElement.injector.get(NgbActiveModal);
    });

    describe('confirmDelete', () => {
      it('Should call delete service on confirmDelete', inject(
        [],
        fakeAsync(() => {
          // GIVEN
          spyOn(service, 'delete').and.returnValue(of({}));

          // WHEN
          comp.confirmDelete(123);
          tick();

          // THEN
          expect(service.delete).toHaveBeenCalledWith(123);
          expect(mockActiveModal.dismissSpy).toHaveBeenCalled();
          expect(mockEventManager.broadcastSpy).toHaveBeenCalled();
        })
      ));
    });
  });
});
