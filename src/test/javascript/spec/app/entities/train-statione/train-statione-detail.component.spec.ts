import { ComponentFixture, TestBed } from '@angular/core/testing';
import { ActivatedRoute } from '@angular/router';
import { of } from 'rxjs';

import { IcyTrainTestModule } from '../../../test.module';
import { TrainStationeDetailComponent } from 'app/entities/train-statione/train-statione-detail.component';
import { TrainStatione } from 'app/shared/model/train-statione.model';

describe('Component Tests', () => {
  describe('TrainStatione Management Detail Component', () => {
    let comp: TrainStationeDetailComponent;
    let fixture: ComponentFixture<TrainStationeDetailComponent>;
    const route = ({ data: of({ trainStatione: new TrainStatione(123) }) } as any) as ActivatedRoute;

    beforeEach(() => {
      TestBed.configureTestingModule({
        imports: [IcyTrainTestModule],
        declarations: [TrainStationeDetailComponent],
        providers: [{ provide: ActivatedRoute, useValue: route }]
      })
        .overrideTemplate(TrainStationeDetailComponent, '')
        .compileComponents();
      fixture = TestBed.createComponent(TrainStationeDetailComponent);
      comp = fixture.componentInstance;
    });

    describe('OnInit', () => {
      it('Should call load all on init', () => {
        // GIVEN

        // WHEN
        comp.ngOnInit();

        // THEN
        expect(comp.trainStatione).toEqual(jasmine.objectContaining({ id: 123 }));
      });
    });
  });
});
