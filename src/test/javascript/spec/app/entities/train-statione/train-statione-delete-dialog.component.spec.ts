import { ComponentFixture, TestBed, inject, fakeAsync, tick } from '@angular/core/testing';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { of } from 'rxjs';
import { JhiEventManager } from 'ng-jhipster';

import { IcyTrainTestModule } from '../../../test.module';
import { TrainStationeDeleteDialogComponent } from 'app/entities/train-statione/train-statione-delete-dialog.component';
import { TrainStationeService } from 'app/entities/train-statione/train-statione.service';

describe('Component Tests', () => {
  describe('TrainStatione Management Delete Component', () => {
    let comp: TrainStationeDeleteDialogComponent;
    let fixture: ComponentFixture<TrainStationeDeleteDialogComponent>;
    let service: TrainStationeService;
    let mockEventManager: any;
    let mockActiveModal: any;

    beforeEach(() => {
      TestBed.configureTestingModule({
        imports: [IcyTrainTestModule],
        declarations: [TrainStationeDeleteDialogComponent]
      })
        .overrideTemplate(TrainStationeDeleteDialogComponent, '')
        .compileComponents();
      fixture = TestBed.createComponent(TrainStationeDeleteDialogComponent);
      comp = fixture.componentInstance;
      service = fixture.debugElement.injector.get(TrainStationeService);
      mockEventManager = fixture.debugElement.injector.get(JhiEventManager);
      mockActiveModal = fixture.debugElement.injector.get(NgbActiveModal);
    });

    describe('confirmDelete', () => {
      it('Should call delete service on confirmDelete', inject(
        [],
        fakeAsync(() => {
          // GIVEN
          spyOn(service, 'delete').and.returnValue(of({}));

          // WHEN
          comp.confirmDelete(123);
          tick();

          // THEN
          expect(service.delete).toHaveBeenCalledWith(123);
          expect(mockActiveModal.dismissSpy).toHaveBeenCalled();
          expect(mockEventManager.broadcastSpy).toHaveBeenCalled();
        })
      ));
    });
  });
});
