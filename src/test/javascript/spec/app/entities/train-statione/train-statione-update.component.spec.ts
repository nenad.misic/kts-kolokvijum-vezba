import { ComponentFixture, TestBed, fakeAsync, tick } from '@angular/core/testing';
import { HttpResponse } from '@angular/common/http';
import { FormBuilder } from '@angular/forms';
import { of } from 'rxjs';

import { IcyTrainTestModule } from '../../../test.module';
import { TrainStationeUpdateComponent } from 'app/entities/train-statione/train-statione-update.component';
import { TrainStationeService } from 'app/entities/train-statione/train-statione.service';
import { TrainStatione } from 'app/shared/model/train-statione.model';

describe('Component Tests', () => {
  describe('TrainStatione Management Update Component', () => {
    let comp: TrainStationeUpdateComponent;
    let fixture: ComponentFixture<TrainStationeUpdateComponent>;
    let service: TrainStationeService;

    beforeEach(() => {
      TestBed.configureTestingModule({
        imports: [IcyTrainTestModule],
        declarations: [TrainStationeUpdateComponent],
        providers: [FormBuilder]
      })
        .overrideTemplate(TrainStationeUpdateComponent, '')
        .compileComponents();

      fixture = TestBed.createComponent(TrainStationeUpdateComponent);
      comp = fixture.componentInstance;
      service = fixture.debugElement.injector.get(TrainStationeService);
    });

    describe('save', () => {
      it('Should call update service on save for existing entity', fakeAsync(() => {
        // GIVEN
        const entity = new TrainStatione(123);
        spyOn(service, 'update').and.returnValue(of(new HttpResponse({ body: entity })));
        comp.updateForm(entity);
        // WHEN
        comp.save();
        tick(); // simulate async

        // THEN
        expect(service.update).toHaveBeenCalledWith(entity);
        expect(comp.isSaving).toEqual(false);
      }));

      it('Should call create service on save for new entity', fakeAsync(() => {
        // GIVEN
        const entity = new TrainStatione();
        spyOn(service, 'create').and.returnValue(of(new HttpResponse({ body: entity })));
        comp.updateForm(entity);
        // WHEN
        comp.save();
        tick(); // simulate async

        // THEN
        expect(service.create).toHaveBeenCalledWith(entity);
        expect(comp.isSaving).toEqual(false);
      }));
    });
  });
});
